﻿using System;
using System.Collections.Generic;
using System.Text;
using System;

namespace DotnetDataLens
{
    public struct GeologyMapRequest
    {
        public GeologyMapRequest(string raw)
        {            
            string[] lines = raw.Split('\n');
            geologyUnits = int.Parse(parseLine(lines[2]));
            geologyUnitsTotal = int.Parse(parseLine(lines[3]));
            code = parseLine(lines[4]);
            unitCode = parseLine(lines[5]);
            mainRock = parseLine(lines[6]);
            subRocks = parseLine(lines[7]);
            mapUnit = parseLine(lines[8]);
            stratigraphicUnit = parseLine(lines[9]);
            stratlex = parseLine(lines[10]);
            sequence = parseLine(lines[11]);
            terrane = parseLine(lines[12]);
            supergroup = parseLine(lines[13]);
            group = parseLine(lines[14]);
            subgroup = parseLine(lines[15]);
            formation = parseLine(lines[16]);
            member = parseLine(lines[17]);
            protolith = parseLine(lines[18]);
            tzone = parseLine(lines[19]);
            stratAge = parseLine(lines[20]);
            absoluteAgeMin = float.Parse(parseLine(lines[21]));
            absoluteAgeMax = float.Parse(parseLine(lines[22]));
            confidence = parseLine(lines[23]);
            description = parseLine(lines[24]);
            rockGroup = parseLine(lines[25]);
            rockClass = parseLine(lines[26]);
            uniqueCode = parseLine(lines[27]);
            textCode = parseLine(lines[28]);
            simpleName = parseLine(lines[29]);
            keyName = parseLine(lines[30]);
            keyGroup = parseLine(lines[31]);
            qmapName = parseLine(lines[32]);
            qmapNumber = int.Parse(parseLine(lines[33]));
        }

        private static string parseLine(string line)
        {
            return line.Split('=')[1].Trim();
        }

        public int geologyUnits { get; }
        public int geologyUnitsTotal { get; }
        public string code { get; }
        public string unitCode { get; }
        public string mainRock { get; }
        public string subRocks { get; }
        public string mapUnit { get; }
        public string stratigraphicUnit { get; }
        public string stratlex { get; }
        public string sequence { get; }
        public string terrane { get; }
        public string supergroup { get; }
        public string group { get; }
        public string subgroup { get; }
        public string formation { get; }
        public string member { get; }
        public string protolith { get; }
        public string tzone { get; }
        public string stratAge { get; }
        public float absoluteAgeMin { get; }
        public float absoluteAgeMax { get; }
        public string confidence { get; }
        public string description { get; }
        public string rockGroup { get; }
        public string rockClass { get; }
        public string uniqueCode { get; }
        public string textCode { get; }
        public string simpleName { get; }
        public string keyName { get; }
        public string keyGroup { get; }
        public string qmapName { get; }
        public int qmapNumber { get; }
    }
}
