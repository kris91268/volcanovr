﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotnetDataLens
{
    public readonly struct FaultMapRequest
    {
        public FaultMapRequest(string raw)
        {
            if (raw.Contains("no features were found"))
            {
                length = 0.0;
                identifier = "";
                accuracy = "";
                dominantSense = "";
                subSense = "";
                activity = "";
                plotRank = 0L;
                name = "no fault found";
                zone = "";
                type = "";
                rdomSense = "";
                rsubSense = "";
                rtype = "";
                dipDir = 0;
                dip = 0;
                age = "";
                totalSlip = "";
                downQuad = "";
                qmapName = "";
                qmapNumber = 0;
                return;
            }
            string[] lines = raw.Split('\n');
            length = double.Parse(parseLine(lines[2]));
            identifier = parseLine(lines[3]);
            accuracy = parseLine(lines[4]);
            dominantSense = parseLine(lines[5]);
            subSense = parseLine(lines[6]);
            activity = parseLine(lines[7]);
            plotRank = long.Parse(parseLine(lines[8]));
            name = parseLine(lines[9]);
            zone = parseLine(lines[10]);
            type = parseLine(lines[11]);
            rdomSense = parseLine(lines[12]);
            rsubSense = parseLine(lines[13]);
            rtype = parseLine(lines[14]);
            dipDir = int.Parse(parseLine(lines[15]));
            dip = int.Parse(parseLine(lines[16]));
            age = parseLine(lines[17]);
            totalSlip = parseLine(lines[18]);
            downQuad = parseLine(lines[19]);
            qmapName = parseLine(lines[20]);
            qmapNumber = int.Parse(parseLine(lines[21]));
        }

        private static string parseLine(string line)
        {
            return line.Split('=')[1].Trim();
        }

        public double length { get; }
        public string identifier { get; }
        public string accuracy { get; }
        public string dominantSense { get; }
        public string subSense { get; }
        public string activity { get; }
        public long plotRank { get; }
        public string name { get; }
        public string zone { get; }
        public string type { get; }
        public string rdomSense { get; }
        public string rsubSense { get; }
        public string rtype { get; }
        public int dipDir { get; }
        public int dip { get; }
        public string age { get; }
        public string totalSlip { get; }
        public string downQuad { get; }
        public string qmapName { get; }
        public int qmapNumber { get; }
    }
}
