﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Drawing;
using System.IO;

namespace DotnetDataLens
{
    public class Map
    {
        private HttpClient client;
        private const string url = "https://maps.gns.cri.nz/geology/wms";
        private int north, east, south, west;
        private const string PROJECTION = "EPSG:2193";

        private const string GEOLOGY_MAP_LAYER = "NZL_GNS_250K_geologic_units";
        private const string FAULT_MAP_LAYER = "gns:NZL_GNS_250K_faults_plotrank";
        private const int SIZE = 2000;

        public byte[] geologyMap;
        public byte[] faultMap;

        public Map(HttpClient client, int northBound, int eastBound, int southBound, int westBound)
        {
            this.client = client;
            north = northBound;
            east = eastBound;
            south = southBound;
            west = westBound;
            getMap();
            getFaults();
        }

        private void getMap()
        {
            string request = buildRequestForMap(GEOLOGY_MAP_LAYER);
            var response = client.GetStreamAsync(request);
            geologyMap = readByteArrayFromStream(response.Result);
        }

        private static byte[] readByteArrayFromStream(Stream stream)
        {
            MemoryStream ms = new MemoryStream();
            stream.CopyTo(ms);
            return ms.ToArray();
        }

        private void getFaults()
        {
            string request = buildRequestForMap(FAULT_MAP_LAYER);
            var response = client.GetStreamAsync(request);
            faultMap = readByteArrayFromStream(response.Result);
        }

        private string buildRequestForMap(string map)
        {
            string request = url;
            request += $"?REQUEST=GetMap&SERVICE=WMS&VERSION=1.3&LAYERS={map}&SYTLES=&FORMAT=image/png&CRS={PROJECTION}";
            request += $"&BBOX={west},{south},{east},{north}&WIDTH={SIZE}&HEIGHT={SIZE}";
            return request;
        }

        private string buildRequestForFeature(string map, int x, int y)
        {
            string request = url;
            request += $"?REQUEST=GetFeatureInfo&SERVICE=WMS&VERSION=1.3&LAYERS={map}&STYLES=&CRS={PROJECTION}&";
            request += $"BBOX={west},{south},{east},{north}&WIDTH={SIZE}&HEIGHT={SIZE}&query_layers={map}&X={x}&Y={y}";
            return request;
        }

        public int getSize()
        {
            return SIZE;
        }

        public GeologyMapRequest getGeologyFeature(int x, int y) // y=0 is top of map 
        {
            string req = buildRequestForFeature(GEOLOGY_MAP_LAYER, x, y);
            var response = client.GetStringAsync(req);
            return new GeologyMapRequest(response.Result);
        }

        public FaultMapRequest getFaultFeature(int x, int y)
        {
            string req = buildRequestForFeature(FAULT_MAP_LAYER, x, y);
            var response = client.GetStringAsync(req);
            return new FaultMapRequest(response.Result);
        }
    }
}
