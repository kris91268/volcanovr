﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;

namespace DotnetDataLens
{
    public class Fits
    {
        [Serializable]
        public class FitsFeatureCollection
        {
            [Serializable]
            public class FitsFeature
            {
                [Serializable]
                public class FitsGeometry
                {
                    public string type;
                    public double[] coordinates;
                }
                [Serializable]
                public class FitsProperties
                {
                    public string siteID;
                    public double height;
                    public double groundRelationship;
                    public string name;
                }
                public string type;
                public FitsGeometry geometry;
                public FitsProperties properties;
            }
            public string type;
            public FitsFeature[] features;
        }

        public readonly struct FitsSite
        {
            public FitsSite(double lt, double ln, double gr, double h, string n, string sid)
            {
                latitude = lt;
                longitude = ln;
                groundRelationship = gr;
                height = h;
                name = n;
                siteID = sid;
            }
            public double latitude { get; }
            public double longitude { get; }
            public double groundRelationship { get; }
            public double height { get; }
            public string name { get; }
            public string siteID { get; }
        }

        private HttpClient client;
        private const string url = "http://fits.geonet.org.nz/";

        public Fits(HttpClient currentClient)
        {
            client = currentClient;
        }

        public static string createWKTPolygon(double startX, double startY, double endX, double endY)
        {
            return $"POLYGON(({startX}+{startY},{endX}+{startY},{endX}+{endY},{startX}+{endY},{startX}+{startY}))";
        }

        public FitsData getDataOfTypeFromSite(string siteID, string typeID)
        {
            try
            {
                var response = client.GetStringAsync(url + $"observation?typeID={typeID}&siteID={siteID}"); // consider streams instead of strings later
                return new FitsData(response.Result, siteID, typeID);
            }
            catch (HttpRequestException exc)
            {
                Console.WriteLine(exc.Message);
                return null;
            }
        }

        public List<FitsSite> getSitesInBounds(double startLat, double startLong, double endLat, double endLong)
        {
            string wkt = createWKTPolygon(startLong, startLat, endLong, endLat);
            var response = client.GetStringAsync(url + $"site?within={wkt}");
            FitsFeatureCollection data = JsonConvert.DeserializeObject<FitsFeatureCollection>(response.Result);
            List<FitsSite> sites = new List<FitsSite>();
            foreach (FitsFeatureCollection.FitsFeature feature in data.features)
            {
                sites.Add(new FitsSite(feature.geometry.coordinates[1], feature.geometry.coordinates[0], feature.properties.groundRelationship,
                    feature.properties.height, feature.properties.name, feature.properties.siteID));
            }
            return sites;
        }

        public FitsData getDataOfTypeFromSiteWithDays(string siteID, string typeID, int daysBeforePresent)
        {
            if (daysBeforePresent > 365000 || daysBeforePresent < 1)
            {
                throw new ArgumentException("Invalid number of days");
            }
            try
            {
                var response = client.GetStringAsync(url + $"observation?typeID={typeID}&siteID={siteID}&days={daysBeforePresent}");
                return new FitsData(response.Result, siteID, typeID);
            } 
            catch (HttpRequestException exc)
            {
                Console.WriteLine(exc.Message);
                return null;
            }
        }

        public FitsData getDataOfTypeFromSiteWithinRange(string siteID, string typeID, DateTime from, DateTime to)
        {
            if (to < from)
            {
                throw new ArgumentException("Dates are reversed");
            }
            TimeSpan span = DateTime.Today - from;
            var response = client.GetStringAsync(url + $"observation?typeID={typeID}&siteID={siteID}&days={span.Days}");
            return new FitsData(cullDaysAfter(response.Result, to), siteID, typeID);
        }

        private static string cullDaysAfter(string data, DateTime date)
        {
            string marker = $"{date.Year}-{date.Month}-{date.Day}";
            string[] stuff = data.Split(new string[] { marker }, StringSplitOptions.None);
            return stuff[0];
        }
    }
}
