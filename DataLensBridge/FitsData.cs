﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DotnetDataLens
{
    public class FitsData
    {
        public readonly struct Column<T>
        {
            public Column(string t, List<T> d)
            {
                title = t;
                data = d;
            }
            public string title { get; }
            public List<T> data { get; }
            public int length()
            {
                return data.Count;
            }
        }

        public readonly struct Entry
        {
            public Entry(DateTime dt, double val, double err)
            {
                dateTime = dt;
                value = val;
                error = err;
            }
            public DateTime dateTime { get; }
            public double value { get; }
            public double error { get; }
        }

        public readonly struct DataBounds
        {
            public DataBounds(double minimum, double maximum)
            {
                min = minimum;
                max = maximum;
            }
            public double min { get; }
            public double max { get; }
        }

        public Column<DateTime> times;
        public List<Column<double>> data;
        public string siteID, typeID;

        public FitsData(string rawData, string siteNameID, string typeName)
        {
            string[] lines = rawData.Split('\n');
            lines = lines.Where((item, index) => item != "").ToArray();
            string[] headings = lines[0].Split(',');
            List<DateTime> datetimes = new List<DateTime>();
            List<double> numbers = new List<double>();
            List<double> error = new List<double>();
            foreach (string line in lines)
            {
                if (line == "" || line.StartsWith("date"))
                {
                    continue;
                }
                string[] parts = line.Split(',');
                datetimes.Add(parseUTCToDateTime(parts[0]));
                numbers.Add(double.Parse(parts[1]));
                error.Add(double.Parse(parts[2]));
            }
            times = new Column<DateTime>(headings[0], datetimes);
            var col1 = new Column<double>(headings[1], numbers);
            var col2 = new Column<double>(headings[2], error);
            data = new List<Column<double>>();
            data.Add(col1);
            data.Add(col2);
            siteID = siteNameID;
            typeID = typeName;
        }

        public Entry getEntryAtLine(int line)
        {
            return new Entry(times.data[line], data[0].data[line], data[1].data[line]);
        }

        public List<Entry> getAllEntries()
        {
            List<Entry> entries = new List<Entry>();
            for (int i = 0; i < times.length(); i++)
            {
                entries.Add(new Entry(times.data[i], data[0].data[i], data[1].data[i]));
            }
            return entries;
        }

        public DataBounds getBoundsOfData(DateTime start, DateTime end)
        {
            if (end <= start)
            {
                throw new ArgumentException("Invalid range of days");
            }
            double min = double.MaxValue;
            double max = double.MinValue;
            for (int i = 0; i < times.data.Count; i++)
            {
                if (times.data[i] < start || times.data[i] > end)
                {
                    continue;
                }
                double d = data[0].data[i];
                if (min >= d)
                {
                    min = d;
                }
                if (max <= d)
                {
                    max = d;
                }
            }
            return new DataBounds(min, max);
        }

        public DataBounds getBoundsOfData()
        {
            return getBoundsOfData(times.data[0], times.data[times.data.Count - 1]);
        }

        private static DateTime parseUTCToDateTime(string utc)
        {
            string[] parts = utc.Split('T');
            string[] date = parts[0].Split('-');
            int year = int.Parse(date[0]);
            int month = int.Parse(date[1]);
            int day = int.Parse(date[2]);
            string[] time = parts[1].Split('.')[0].Split(':');
            int hour = int.Parse(time[0]);
            int minute = int.Parse(time[1]);
            int second = int.Parse(time[2]);
            return new DateTime(year, month, day, hour, minute, second);
        }
    }
}
