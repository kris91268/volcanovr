﻿using System;
using System.Collections.Generic;
using System.Net.Http;

namespace DotnetDataLens
{
    public class Fdsn
    {
        public readonly struct FdsnEvent
        {
            public FdsnEvent(string eid, string t, double lt, double ln, double d, string a, string cat, 
                string cont, string contID, string mt, float m, string mauth, string eln)
            {
                eventID = eid;
                time = t;
                latitude = lt;
                longitude = ln;
                depth = d;
                author = a;
                catalog = cat;
                contributor = cont;
                contributorID = contID;
                magType = mt;
                magnitude = m;
                magAuthor = mauth;
                eventLocationName = eln;
            }
            public string eventID { get; }
            public string time { get; }
            public double latitude { get; }
            public double longitude { get; }
            public double depth { get; }
            public string author { get; }
            public string catalog { get; }
            public string contributor { get; }
            public string contributorID { get; }
            public string magType { get; }
            public float magnitude { get; }
            public string magAuthor { get; }
            public string eventLocationName { get; }
        }

        private HttpClient client;
        private static string url = "https://service.geonet.org.nz/fdsnws/event/1/";

        public Fdsn(HttpClient currentClient)
        {
            client = currentClient;
        }

        private List<FdsnEvent> requestAndParseData(string requestString)
        {
            var request = client.GetStringAsync(url + requestString);
            string data = request.Result;
            string[] lines = data.Split('\n');
            List<FdsnEvent> events = new List<FdsnEvent>();
            for (int i = 1; i < lines.Length; i++)
            {
                string[] split = lines[i].Split('|');
                if (split.Length < 13)
                {
                    continue;
                }
                string eventID = split[0];
                string time = split[1];
                double latitude = double.Parse(split[2]);
                double longitude = double.Parse(split[3]);
                double depth = double.Parse(split[4]);
                string author = split[5];
                string catalog = split[6];
                string contributor = split[7];
                string contributorID = split[8];
                string magType = split[9];
                float magnitude = float.Parse(split[10]);
                string magAuthor = split[11];
                string eventLocationName = split[12];
                events.Add(new FdsnEvent(eventID, time, latitude, longitude, depth, author, catalog, contributor, contributorID, magType, magnitude, magAuthor, eventLocationName));
            }
            return events;
        }

        public List<FdsnEvent> getEventsWithinTimeAndBounds(string startTime, string endTime, double minLat, double minLong, double maxLat, double maxLong)
        {
            if (minLat >= maxLat || minLong >= maxLong)
            {
                throw new ArgumentException("Invalid bounds");
            }
            return requestAndParseData($"query?minlatitude={minLat}&minlongitude={minLong}&maxlatitude={maxLat}&maxlongitude={maxLong}"
                + $"&starttime={startTime}&endtime={endTime}&format=text");
        }

        public List<FdsnEvent> getEventsWithinTimeBoundsAndMagnitude(string startTime, string endTime, double minLat, double minLong, double maxLat, double maxLong,
            float minMag, float maxMag)
        {
            if (minLat >= maxLat || minLong >= maxLong || minMag >= maxMag)
            {
                throw new ArgumentException("Invalid bounds");
            }
            if (minMag < -2.0f || maxMag > 10.0f)
            {
                throw new ArgumentException("Invalid magnitude range");
            }
            string request = $"query?minlatitude={minLat}&minlongitude={minLong}&maxlatitude={maxLat}&maxLongitude={maxLong}"
                + $"&starttime={startTime}&endtime={endTime}&minmagnitude={minMag}&maxmagnitude={maxMag}";
            return requestAndParseData(request);
        }

        public List<FdsnEvent> getEventsFromCustomQuery(string query)
        {
            return requestAndParseData(query);
        }
    }
}
