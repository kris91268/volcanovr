﻿using System;
using System.Collections.Generic;
using System.Net.Http;

namespace DotnetDataLens
{
    public class Datalens
    {
        public Fits fits;
        public Fdsn fdsn;

        private HttpClient client;

        public Datalens()
        {
            var client = new HttpClient();
            fits = new Fits(client);
            fdsn = new Fdsn(client);
            this.client = client;
        }

        public Map createMap(int west, int north, int south, int east)
        {
            return new Map(client, north, east, south, west);
        }
    }
}
