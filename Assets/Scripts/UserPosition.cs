using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserPosition : MonoBehaviour
{
    public enum CoordinateSystem
    {
        NZGD2000,
        NZTM
    }

    // z is easting, x is northing
    public Transform cameraTransform;
    public CoordinateSystem displayedSystem;

    public double latitude { get; private set; }
    public double longitude { get; private set; }
    public int depth { get; private set; }

    private float timeSinceLastUpdate = 0.0f;

    private int originNorthing;
    private int originEasting;
    private int widthMeters;
    private double ratio;
    private double heightRatio;
    private Transform terrainTransform;

    void Start()
    {
        double origLat = double.Parse(PlayerPrefs.GetString("latitude"));
        double origLong = double.Parse(PlayerPrefs.GetString("longitude"));
        double[] nztm = CoordinateConverter.geographic_to_transverse_mercator(origLat, origLong);
        int width = PlayerPrefs.GetInt("width");
        widthMeters = width * 1000;
        originEasting = (int)nztm[0] - (widthMeters / 2);
        originNorthing = (int)nztm[1] + (widthMeters / 2);
        ratio = 1000.0 / (double)(widthMeters);
        terrainTransform = GameObject.Find("Terrain").GetComponent<Transform>();
        heightRatio = GameObject.Find("Terrain").GetComponent<Terrain>().terrainData.size.x / (width * 1000.0);
    }

    void Update()
    {
        timeSinceLastUpdate += Time.deltaTime;
        if (timeSinceLastUpdate >= 0.5f)
        {
            timeSinceLastUpdate = 0.0f;
            computeLocation();
            computeDepth();
        }
    }

    private void computeLocation()
    {
        float deltaZ = terrainTransform.position.z + cameraTransform.position.z;
        float deltaX = terrainTransform.position.x + cameraTransform.position.x;
        latitude = originNorthing - (deltaX * ratio);
        longitude = originEasting + (deltaZ * ratio);
        if (displayedSystem == CoordinateSystem.NZGD2000)
        {
            double[] nzgd = CoordinateConverter.transverse_mercator_to_geographic((int)longitude, (int)latitude);
            longitude = nzgd[0];
            latitude = nzgd[1];
        }
    }

    private void computeDepth()
    {
        depth = (int)(cameraTransform.position.y * heightRatio);
        depth *= 1000;
    }
}
