using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class VRLaserPointer : MonoBehaviour
{
    public float defaultLength;
    protected LineRenderer lineRenderer;
    public GameObject cursor;


    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();

    }

    // Update is called once per frame
    void Update()
    {
        lineRenderer.SetPosition(0, transform.position);
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.forward);
        Physics.Raycast(ray, out hit);
        Vector3 end = transform.position + (transform.forward * defaultLength);
        if (hit.collider)
        {
            end = hit.point;
        }
        lineRenderer.SetPosition(1, end);
        cursor.transform.position = end;
    }
}
