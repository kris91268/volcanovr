using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Valve.VR;

public class ChangeMenuFocusPC : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (SteamVR.active)
        {
            Debug.Log("SteamVR is active, Disable click menu focus");
            Destroy(GetComponent<ChangeMenuFocusPC>());
        }
    }

    GraphicRaycaster raycaster;

    public GameObject mainCamera;

    private void Awake()
    {
        this.raycaster = GetComponent<GraphicRaycaster>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            PointerEventData data = new PointerEventData(EventSystem.current);
            List<RaycastResult> results = new List<RaycastResult>();
            data.position = Input.mousePosition;
            this.raycaster.Raycast(data, results);
            if (results.Count > 0)
            {
                string name = results[0].gameObject.transform.name;
                if (name.Equals("Panel"))
                {
                    mainCamera.transform.LookAt(results[0].gameObject.transform);
                }
            }
        }
    }
}
