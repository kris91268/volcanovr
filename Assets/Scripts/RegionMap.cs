using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class RegionMap : MonoBehaviour
{
    // 1769449.618 E	5832505.864 N
    private const int TOP_EASTING = 1769450;
    private const int TOP_NORTHING = 5832506;
    // 1996058 E    5570370 N
    private const int BOTTOM_EASTING = 1996058;
    private const int BOTTOM_NORTHING = 5570370;

    private const int IMAGE_WIDTH = 250;
    private const int IMAGE_HEIGHT = 150;

    private const double WIDTH_RATIO = (double)IMAGE_WIDTH / (BOTTOM_EASTING - TOP_EASTING);
    private const double HEIGHT_RATIO = (double)IMAGE_HEIGHT / (TOP_NORTHING - BOTTOM_NORTHING);

    public Image image;
    public InputField latitudeField, longitudeField;
    public InputField widthField;
    public LineRenderer lines;

    private const int Z_OFFSET = -2;

    void Start()
    {
        latitudeField.onValueChanged.AddListener(delegate { updateLines(); });
        longitudeField.onValueChanged.AddListener(delegate { updateLines(); });
        widthField.onValueChanged.AddListener(delegate { updateLines(); });
        updateLines();
    }

    private void updateLines()
    {
        double latitude, longitude;
        int width;
        try
        {
            latitude = double.Parse(latitudeField.text);
            longitude = double.Parse(longitudeField.text);
            width = int.Parse(widthField.text);
        }
        catch (Exception exc)
        {
            Debug.Log(exc.Message);
            for (int i = 0; i < 4; i++)
            {
                lines.SetPosition(i, Vector3.zero);
            }
            return;
        }
        calculateLinePositions(latitude, longitude, width);
    }

    private void calculateLinePositions(double latitude, double longitude, int width)
    {
        double[] nztm = CoordinateConverter.geographic_to_transverse_mercator(latitude, longitude);
        int deltaE = Math.Abs((int)nztm[0] - TOP_EASTING);
        int deltaN = Math.Abs((int)nztm[1] - TOP_NORTHING);
        int topLeftXAbsolute = -(IMAGE_WIDTH / 2);
        int topLeftYAbsolute = (IMAGE_HEIGHT / 2);
        int topLeftX = topLeftXAbsolute + (int)(deltaE * WIDTH_RATIO) - (int)((width * 500) * WIDTH_RATIO);
        int topLeftY = topLeftYAbsolute - (int)(deltaN * HEIGHT_RATIO) + (int)((width * 500) * HEIGHT_RATIO);
        int bottomRightX = topLeftX + (int)((width * 1000) * WIDTH_RATIO);
        int bottomRightY = topLeftY - (int)((width * 1000) * HEIGHT_RATIO);
        lines.SetPosition(0, new Vector3(topLeftX, topLeftY, Z_OFFSET));
        lines.SetPosition(1, new Vector3(bottomRightX, topLeftY, Z_OFFSET));
        lines.SetPosition(2, new Vector3(bottomRightX, bottomRightY, Z_OFFSET));
        lines.SetPosition(3, new Vector3(topLeftX, bottomRightY, Z_OFFSET));
    }

    /*
     * 
        int topLeftX = -(IMAGE_WIDTH / 2) + (int)(deltaE / WIDTH_RATIO);
        int topLeftY = -(IMAGE_HEIGHT / 2) + (int)(deltaN / HEIGHT_RATIO);
        int bottomRightX = -(IMAGE_WIDTH / 2) + (int)((deltaE + (width * 1000)) / WIDTH_RATIO);
        int bottomRightY = -(IMAGE_HEIGHT / 2) + (int)((deltaN + (width * 1000)) / HEIGHT_RATIO);
    */
}
