using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DotnetDataLens;
using System;

public class TimeGraph
{
    public class RangeConversion
    {
        private double firstMax { get; }
        private double firstMin { get; }
        private int secondMax { get; }
        private int secondMin { get; }

        public RangeConversion(double maxFirst, double minFirst, int maxSecond, int minSecond)
        {
            firstMax = maxFirst;
            firstMin = minFirst;
            secondMax = maxSecond;
            secondMin = minSecond;
        }

        public int evaluateSecond(double value)
        {
            if (value < firstMin || value > firstMax)
            {
                throw new ArgumentException("Invalid value for range");
            }
            double firstRange = firstMax - firstMin;
            int secondRange = secondMax - secondMin;
            return (int)(((value - firstMin) * secondRange) / firstRange) + secondMin;
        }

        public double evaluateFirst(int value)
        {
            if (value < secondMin || value > secondMax)
            {
                throw new ArgumentException("Invalid value for range");
            }
            double firstRange = firstMax - firstMin;
            int secondRange = secondMax - secondMin;
            return (((value - secondMin) * firstRange) / secondRange) + firstMin;
        }
    }

    public class DateRangeConversion
    {
        public DateTime start { get; }
        public DateTime end { get; }
        public int max { get; }
        public int min { get; }

        public DateRangeConversion(DateTime start, DateTime end, int max, int min)
        {
            this.start = start;
            this.end = end;
            this.max = max;
            this.min = min;
        }

        public int evaluate(DateTime instance)
        {
            long dateRange = end.Ticks - start.Ticks;
            int valueRange = max - min;
            return (int)(((instance.Ticks - start.Ticks) * valueRange) / dateRange) + min;
        }
    }

    // graph constants
    private static int TEX_WIDTH = 1080;
    private static int TEX_HEIGHT = 720;
    private static float AXES_INSET_PERCENTAGE = 0.1f;

    private FitsData graphData;

    private Texture2D graph;
    
    public TimeGraph(FitsData data)
    {
        graphData = data;
        generateGraph();
    }

    public void generateGraph()
    {
        Texture2D texture = new Texture2D(TEX_WIDTH, TEX_HEIGHT);
        makeCanvasWhite(texture);
        drawLine(texture, Color.black, new Vector2(TEX_WIDTH * AXES_INSET_PERCENTAGE, TEX_HEIGHT * AXES_INSET_PERCENTAGE),
            new Vector2(TEX_WIDTH * AXES_INSET_PERCENTAGE, TEX_HEIGHT - (TEX_HEIGHT * AXES_INSET_PERCENTAGE)), 4);
        drawLine(texture, Color.black, new Vector2(TEX_WIDTH * AXES_INSET_PERCENTAGE, TEX_HEIGHT * AXES_INSET_PERCENTAGE),
            new Vector2(TEX_WIDTH, TEX_HEIGHT * AXES_INSET_PERCENTAGE), 4);
        GraphTextMaker textRenderer = new GraphTextMaker(texture);
        textRenderer.drawText($"{graphData.typeID} data for site {graphData.siteID}", GraphTextMaker.Alignment.CENTER,
            new Vector2Int(TEX_WIDTH / 2, TEX_HEIGHT - ((int)(TEX_HEIGHT * AXES_INSET_PERCENTAGE) / 2) - 10), 2.25f);
        var bounds = graphData.getBoundsOfData();
        RangeConversion rc = new RangeConversion(bounds.max, bounds.min, TEX_HEIGHT - (int)(TEX_HEIGHT * AXES_INSET_PERCENTAGE), (int)(TEX_HEIGHT * AXES_INSET_PERCENTAGE));
        DateRangeConversion drc = new DateRangeConversion(graphData.times.data[0], graphData.times.data[graphData.times.data.Count - 1],
            (int)(TEX_WIDTH * AXES_INSET_PERCENTAGE), TEX_WIDTH);
        Vector2 previousPoint = new Vector2(drc.evaluate(graphData.times.data[0]), rc.evaluateSecond(graphData.data[0].data[0]));
        for (int i = 1; i < graphData.data[0].data.Count; i++)
        {
            Vector2 currentPoint = new Vector2(drc.evaluate(graphData.times.data[i]), rc.evaluateSecond(graphData.data[0].data[i]));
            drawLine(texture, Color.red, previousPoint, currentPoint, 3);
            previousPoint = currentPoint;
        }
        textRenderer.drawText("Date", GraphTextMaker.Alignment.CENTER, new Vector2Int(TEX_WIDTH / 2, (int)(TEX_HEIGHT * AXES_INSET_PERCENTAGE) / 2), 1.5f);
        textRenderer.drawText(graphData.typeID, GraphTextMaker.Alignment.CENTER, new Vector2Int((int)(TEX_WIDTH * AXES_INSET_PERCENTAGE) / 2, TEX_HEIGHT / 2), 1.5f);
        textRenderer.drawText(bounds.max.ToString(), GraphTextMaker.Alignment.RIGHT, new Vector2Int((int)(TEX_WIDTH * AXES_INSET_PERCENTAGE) - 2, (int)(TEX_HEIGHT - (TEX_HEIGHT * AXES_INSET_PERCENTAGE))), 1.5f);
        textRenderer.drawText(bounds.min.ToString(), GraphTextMaker.Alignment.RIGHT, new Vector2Int((int)(TEX_WIDTH * AXES_INSET_PERCENTAGE) - 2, (int)(TEX_HEIGHT * AXES_INSET_PERCENTAGE)), 1.5f);
        Vector2Int leftOnYAxis = new Vector2Int((int)(TEX_WIDTH * AXES_INSET_PERCENTAGE), (int)(TEX_HEIGHT * AXES_INSET_PERCENTAGE));
        textRenderer.drawText(formatDate(graphData.times.data[0]), GraphTextMaker.Alignment.CENTER, new Vector2Int(leftOnYAxis.x, leftOnYAxis.y - 40), 1.5f);
        drawLine(texture, Color.black, new Vector2(leftOnYAxis.x, leftOnYAxis.y), new Vector2(leftOnYAxis.x, leftOnYAxis.y - 40.0f), 1);
        Vector2Int rightOnYAxis = new Vector2Int(TEX_WIDTH, (int)(TEX_HEIGHT * AXES_INSET_PERCENTAGE));
        textRenderer.drawText(formatDate(graphData.times.data[graphData.times.data.Count - 1]), GraphTextMaker.Alignment.RIGHT, new Vector2Int(rightOnYAxis.x - 20, rightOnYAxis.y - 40), 1.5f);
        drawLine(texture, Color.black, new Vector2(rightOnYAxis.x, rightOnYAxis.y), new Vector2(rightOnYAxis.x - 20.0f, rightOnYAxis.y - 40.0f), 1);
        texture.Apply();
        graph = texture;
    }

    private static string formatDate(DateTime date)
    {
        return $"{date.Day}/{date.Month}/{date.Year}";
    }

    private static void makeCanvasWhite(Texture2D tex)
    {
        for (int i = 0; i < tex.width; i++)
        {
            for (int j = 0; j < tex.height; j++)
            {
                tex.SetPixel(i, j, Color.white);
            }
        }
    }

    public Texture2D getGraph()
    {
        return graph;
    }
    
    // 0, 0 is at bottom left of texture2d
    private static void drawLine(Texture2D texture, Color color, Vector2 start, Vector2 end, int widthPixels)
    {
        Vector2 t = start;
        float frac = 1 / Mathf.Sqrt(Mathf.Pow(end.x - start.x, 2) + Mathf.Pow(end.y - start.y, 2));
        float ctr = 0;
        while ((int)t.x != (int)end.x || (int)t.y != (int)end.y)
        {
            t = Vector2.Lerp(start, end, ctr);
            ctr += frac;
            texture.SetPixel((int)t.x, (int)t.y, color);
        }
    }
}
