using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MainMenuHandler : MonoBehaviour
{
    public readonly struct Pair<T, K>
    {
        public Pair(T f, K s)
        {
            first = f;
            second = s;
        }
        public T first { get; }
        public K second { get; }
    }

    private static Dictionary<string, Pair<string, string>> places = new Dictionary<string, Pair<string, string>>()
    {
        { "Okitania", new Pair<string, string>("176.426433", "-38.193602") },
        { "Rotorua", new Pair<string, string>("176.268087", "-38.078341") },
        { "Reporora", new Pair<string, string>("176.344444", "-38.429472") },
        { "Kapenga", new Pair<string, string>("176.185912", "-38.247071") },
        { "Whakamaru", new Pair<string, string>("175.944566", "-38.490724") },
        { "Taupo", new Pair<string, string>("175.915874", "-38.782911") },
        { "Tongariro", new Pair<string, string>("175.651249", "-39.138576") },
        { "Ruapehu", new Pair<string, string>("175.562727", "-39.279757") }
    };

    public class DateRangeException : System.Exception
    {
        public DateRangeException()
        {

        }
        public DateRangeException(string message) : base(message)
        {

        }
        public DateRangeException(string message, Exception inner) : base(message, inner)
        {

        }
    }
    // Start is called before the first frame update
    void Start()
    {
        // PlayerPrefs.DeleteAll();
    }

    public void loadButtonPressed(GameObject menu)
    {
        Debug.Log("Load button pressed!");
        var panel = getChildWithName(menu, "Panel");
        string latitudeInput = getChildWithName(getChildWithName(panel, "LatitudeInputField"), "Text").GetComponent<UnityEngine.UI.Text>().text;
        string longitudeInput = getChildWithName(getChildWithName(panel, "LongitudeInputField"), "Text").GetComponent<UnityEngine.UI.Text>().text;
        double latitude = 0.0;
        double longitude = 0.0;
        try
        {
            latitude = Double.Parse(latitudeInput);
            longitude = Double.Parse(longitudeInput);
        }
        catch (System.Exception exc)
        {
            getChildWithName(panel, "ErrorText").GetComponent<UnityEngine.UI.Text>().text = "Invalid latitude and/or longitude";
            Debug.Log(exc.Message);
            return;
        }
        string widthInput = getChildWithName(getChildWithName(panel, "WidthInputField"), "Text").GetComponent<UnityEngine.UI.Text>().text;
        uint width = 0;
        try
        {
            width = UInt32.Parse(widthInput);
        }
        catch (System.Exception exc)
        {
            getChildWithName(panel, "ErrorText").GetComponent<UnityEngine.UI.Text>().text = "Invalid width";
            Debug.Log(exc.Message);
            return;
        }
        string fromDateInput = getChildWithName(getChildWithName(panel, "StartDateInputField"), "Text").GetComponent<UnityEngine.UI.Text>().text;
        string toDateInput = getChildWithName(getChildWithName(panel, "EndDateInputField"), "Text").GetComponent<UnityEngine.UI.Text>().text;
        DateTime[] dates = null;
        try
        {
            dates = validateDateRange(fromDateInput, toDateInput);
        }
        catch (DateRangeException exc)
        {
            getChildWithName(panel, "ErrorText").GetComponent<UnityEngine.UI.Text>().text = exc.Message;
            return;
        }
        PlayerPrefs.SetString("latitude", latitudeInput);
        PlayerPrefs.SetString("longitude", longitudeInput);
        PlayerPrefs.SetInt("width", (int)width);
        PlayerPrefs.SetString("from_date", fromDateInput);
        PlayerPrefs.SetString("to_date", toDateInput);
        int data = 3;
        if (getChildWithName(panel, "EarthquakesToggle").GetComponent<UnityEngine.UI.Toggle>().isOn)
        {
            data |= 0b1;
        }
        if (getChildWithName(panel, "DeformationToggle").GetComponent<UnityEngine.UI.Toggle>().isOn)
        {
            data |= 0b10;
        }
        PlayerPrefs.SetInt("data", data);
        PlayerPrefs.SetString("preset", "none");
        PlayerPrefs.SetString("volcano", "taupo"); // Placeholder for now.
        PlayerPrefs.Save();
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }

    public void onAreaDropdownChanged()
    {
        Dropdown drop = getChildWithName(transform.GetChild(0).gameObject, "AreaDropdown").GetComponent<Dropdown>();
        string selected = drop.options[drop.value].text; 
        if (selected.Equals("Custom"))
        {
            return;
        }
        InputField latitudeField = getChildWithName(transform.GetChild(0).gameObject, "LatitudeInputField").GetComponent<InputField>();
        InputField longitudeField = getChildWithName(transform.GetChild(0).gameObject, "LongitudeInputField").GetComponent<InputField>();
        latitudeField.text = places[selected].second;
        longitudeField.text = places[selected].first;
    }

    private static DateTime[] validateDateRange(string from, string to)
    {
        int fromDay, fromMonth, fromYear;
        int toDay, toMonth, toYear;
        string[] fromSplit = from.Split('/');
        string[] toSplit = to.Split('/');
        try
        {
            fromDay = Int32.Parse(fromSplit[0]);
            fromMonth = Int32.Parse(fromSplit[1]);
            fromYear = Int32.Parse(fromSplit[2]);
            toDay = Int32.Parse(toSplit[0]);
            toMonth = Int32.Parse(toSplit[1]);
            toYear = Int32.Parse(toSplit[2]);
        }
        #pragma warning disable 168
        catch (Exception exc)
        {
            throw new DateRangeException("Invalid date formats");
        }
        if (fromDay > 31 || toDay > 31 || fromMonth > 12 || toMonth > 12)
        {
            throw new DateRangeException("Invalid month or day");
        }
        var fromDate = new DateTime(fromYear, fromMonth, fromDay);
        var toDate = new DateTime(toYear, toMonth, toDay);
        if (fromDate >= toDate)
        {
            throw new DateRangeException("Start date is after the end date");
        }
        int days = toDate.Subtract(fromDate).Days;
        if (days > (365 * 2)) // so the DataLens doesn't pull too much data at once. Can be changed to a different number of days
        {
            throw new DateRangeException("Number of days between dates too big");
        }
        return new DateTime[] { fromDate, toDate};
    }

    private static GameObject getChildWithName(GameObject parent, string childname)
    {
        for (int i = 0; i < parent.transform.childCount; i++)
        {
            if (parent.transform.GetChild(i).transform.name == childname)
            {
                return parent.transform.GetChild(i).gameObject;
            }
        }
        return null;
    }
}
