using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VRNumberKeyboardHandler : MonoBehaviour
{
    public Camera eventCamera;
    public VRInput vrInput;
    [HideInInspector]
    public InputField currentInputField;

    // Start is called before the first frame update
    void Start()
    {
        this.transform.GetChild(0).GetComponent<Canvas>().worldCamera = eventCamera;
        currentInputField.caretPosition = currentInputField.text.Length;
    }

    private void enterCharacter(char character)
    {
        int pos = currentInputField.caretPosition;
        string text = currentInputField.text;
        currentInputField.text = text.Insert(pos, character.ToString());
        currentInputField.caretPosition++;
    }

    public void onButton0Press()
    {
        enterCharacter('0');
    }

    public void onButton1Press()
    {
        enterCharacter('1');
    }

    public void onButton2Press()
    {
        enterCharacter('2');
    }

    public void onButton3Press()
    {
        enterCharacter('3');
    }

    public void onButton4Press()
    {
        enterCharacter('4');
    }

    public void onButton5Press()
    {
        enterCharacter('5');
    }

    public void onButton6Press()
    {
        enterCharacter('6');
    }
    public void onButton7Press()
    {
        enterCharacter('7');
    }

    public void onButton8Press()
    {
        enterCharacter('8');
    }

    public void onButton9Press()
    {
        enterCharacter('9');
    }

    public void onButtonSlashPress()
    {
        enterCharacter('/');
    }

    public void onButtonDotPress()
    {
        enterCharacter('.');
    }

    public void onButtonMinusPress()
    {
        enterCharacter('-');
    }

    public void onButtonBackspacePress()
    {
        currentInputField.text.Remove(currentInputField.caretPosition - 1, 1);
    }

    public void onButtonClearPress()
    {
        Debug.Log("clearing text");
        currentInputField.text = "";
    }

    public void onButtonOKPress()
    {
        Debug.Log("Destroying keyboard prefab");
        Destroy(this.gameObject);
    }
}
