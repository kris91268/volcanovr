using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/*
 * https://www.linz.govt.nz/data/geodetic-services/coordinate-conversion/projection-conversions/transverse-mercator-transformation-formulae
 * 
 * Adapted from code from https://www.linz.govt.nz/data/geodetic-services/download-geodetic-software#nztm2000
 */
public class CoordinateConverter
{
    private const double PI = Math.PI;
    private const double TWOPI = 2 * PI;
    private const double rad2deg = 180 / PI;

    public struct TMProjection
    {
        public double meridian { get; set; }
        public double scalef { get; set; }
        public double orglat { get; set; }
        public double falsee { get; set; }
        public double falsen { get; set; }
        public double utom { get; set; }
        public double a { get; set; }
        public double rf { get; set; }
        public double f { get; set; }
        public double e2 { get; set; }
        public double ep2 { get; set; }
        public double om { get; set; }
    }
    
    private static double meridian_arc(TMProjection proj, double lt)
    {
        double e2 = proj.e2;
        double a = proj.a;
        double e4 = e2 * e2;
        double e6 = e4 * e2;

        double A0 = 1 - (e2 / 4.0) - (3.0 * e4 / 64.0) - (5.0 * e6 / 256.0);
        double A2 = (3.0 / 8.0) * (e2 + e4 / 4.0 + 15.0 * e6 / 128.0);
        double A4 = (15.0 / 256.0) * (e4 + 3.0 * e6 / 4.0);
        double A6 = 35.0 * e6 / 3072.0;

        return a * (A0 * lt - A2 * sin(2 * lt) + A4 * sin(4 * lt) - A6 * sin(6 * lt));
    }

    private static TMProjection get_mercator_projection(double a, double rf, double cm, double sf, double lto, double fe, double fn, double utom)
    {
        double f;
        TMProjection proj = new TMProjection();
        proj.meridian = cm;
        proj.scalef = sf;
        proj.orglat = lto;
        proj.falsee = fe;
        proj.falsen = fn;
        proj.utom = utom;
        if (rf != 0.0) f = 1.0 / rf; else f = 0.0;
        proj.a = a;
        proj.rf = rf;
        proj.f = f;
        proj.e2 = 2.0 * f - f * f;
        proj.ep2 = proj.e2 / (1.0 - proj.e2);

        proj.om = meridian_arc(proj, proj.orglat);
        return proj;
    }

    public static double[] geographic_to_transverse_mercator(double lt, double ln)
    {
        if (lt > 0.0 || ln < 173.0 || lt < -90.0 || ln > 180.0)
        {
            throw new ArgumentException("Invalid latitude and/or longitude");
        }
        lt /= rad2deg; // convert input to radians
        ln /= rad2deg;
        TMProjection proj = get_mercator_projection(6378137, 298.257222101, 173.0 / rad2deg, 0.9996, 0.0 / rad2deg, 1600000.0, 10000000.0, 1.0);
        double fn = proj.falsen;
        double fe = proj.falsee;
        double sf = proj.scalef;
        double e2 = proj.e2;
        double a = proj.a;
        double cm = proj.meridian;
        double om = proj.om;
        double utom = proj.utom;

        double dlon = ln - cm;
        while (dlon > PI) dlon -= TWOPI;
        while (dlon < -PI) dlon += TWOPI;

        double m = meridian_arc(proj, lt);

        double slt = sin(lt);

        double eslt = (1.0 - e2 * slt * slt);
        double eta = a / Math.Sqrt(eslt);
        double rho = eta * (1.0 - e2) / eslt;
        double psi = eta / rho;

        double clt = cos(lt);
        double w = dlon;

        double wc = clt * w;
        double wc2 = wc * wc;

        double t = slt / clt;
        double t2 = t * t;
        double t4 = t2 * t2;
        double t6 = t2 * t4;

        double trm1 = (psi - t2) / 6.0;

        double trm2 = (((4.0 * (1.0 - 6.0 * t2) * psi + (1.0 + 8.0 * t2)) * psi - 2.0 * t2) * psi + t4) / 120.0;

        double trm3 = (61 - 479.0 * t2 + 179.0 * t4 - t6) / 5040.0;

        double gce = (sf * eta * dlon * clt) * (((trm3 * wc2 + trm2) * wc2 + trm1) * wc2 + 1.0);
        double ce = gce / utom + fe;

        trm1 = 1.0 / 2.0;

        trm2 = ((4.0 * psi + 1) * psi - t2) / 24.0;

        trm3 = ((((8.0 * (11.0 - 24.0 * t2) * psi - 28.0 * (1.0 - 6.0 * t2)) * psi + (1.0 - 32.0 * t2)) * psi - 2.0 * t2) * psi + t4) / 720.0;

        double trm4 = (1385.0 - 3111.0 * t2 + 543.0 * t4 - t6) / 40320.0;

        double gcn = (eta * t) * ((((trm4 * wc2 + trm3) * wc2 + trm2) * wc2 + trm1) * wc2);
        double cn = (gcn + m - om) * sf / utom + fn;
        return new double[] { ce, cn };
    }
    private static double foot_point_lat(TMProjection tm, double m)
    {
        double f = tm.f;
        double a = tm.a;

        double n = f / (2.0 - f);
        double n2 = n * n;
        double n3 = n2 * n;
        double n4 = n2 * n2;

        double g = a * (1.0 - n) * (1.0 - n2) * (1 + 9.0 * n2 / 4.0 + 225.0 * n4 / 64.0);
        double sig = m / g;

        double phio = sig + (3.0 * n / 2.0 - 27.0 * n3 / 32.0) * sin(2.0 * sig) + (21.0 * n2 / 16.0 - 55.0 * n4 / 32.0) * sin(4.0 * sig)
                        + (151.0 * n3 / 96.0) * sin(6.0 * sig) + (1097.0 * n4 / 512.0) * sin(8.0 * sig);
        return phio;
    }

    public static double[] transverse_mercator_to_geographic(int ce, int cn)
    {
        if (ce < 1600000 || ce > 2266925 || cn < 4863201 || cn > 6363201)
        {
            throw new ArgumentException("Invalid easting and/or northing");
        }
        TMProjection proj = get_mercator_projection(6378137, 298.257222101, 173.0 / rad2deg, 0.9996, 0.0 / rad2deg, 1600000.0, 10000000.0, 1.0);
        double fn = proj.falsen;
        double fe = proj.falsee;
        double sf = proj.scalef;
        double e2 = proj.e2;
        double a = proj.a;
        double cm = proj.meridian;
        double om = proj.om;
        double utom = proj.utom;

        double cn1 = (cn - fn) * utom / sf + om;
        double fphi = foot_point_lat(proj, cn1);
        double slt = sin(fphi);
        double clt = cos(fphi);

        double eslt = (1.0 - e2 * slt * slt);
        double eta = a / Math.Sqrt(eslt);
        double rho = eta * (1.0 - e2) / eslt;
        double psi = eta / rho;

        double E = (ce - fe) * utom;
        double x = E / (eta * sf);
        double x2 = x * x;


        double t = slt / clt;
        double t2 = t * t;
        double t4 = t2 * t2;

        double trm1 = 1.0 / 2.0;

        double trm2 = ((-4.0 * psi + 9.0 * (1 - t2)) * psi + 12.0 * t2) / 24.0;

        double trm3 = ((((8.0 * (11.0 - 24.0 * t2) * psi - 12.0 * (21.0 - 71.0 * t2)) * psi
                      + 15.0 * ((15.0 * t2 - 98.0) * t2 + 15)) * psi + 180.0 * ((-3.0 * t2 + 5.0) * t2)) * psi + 360.0 * t4) / 720.0;

        double trm4 = (((1575.0 * t2 + 4095.0) * t2 + 3633.0) * t2 + 1385.0) / 40320.0;

        double lt = fphi + (t * x * E / (sf * rho)) * (((trm4 * x2 - trm3) * x2 + trm2) * x2 - trm1);

        trm1 = 1.0;

        trm2 = (psi + 2.0 * t2) / 6.0;

        trm3 = (((-4.0 * (1.0 - 6.0 * t2) * psi + (9.0 - 68.0 * t2)) * psi + 72.0 * t2) * psi + 24.0 * t4) / 120.0;

        trm4 = (((720.0 * t2 + 1320.0) * t2 + 662.0) * t2 + 61.0) / 5040.0;

        double ln = cm - (x / clt) * (((trm4 * x2 - trm3) * x2 + trm2) * x2 - trm1);
        return new double[] { ln * rad2deg, lt * rad2deg};
    }

    private static double sin(double radians)
    {
        return Math.Sin(radians);
    }

    private static double cos(double radians)
    {
        return Math.Cos(radians);
    }
}
