using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcedualCubes : MonoBehaviour
{
    public Color[] cubeColours;
    public Material cubeMaterial;
    public int cubeSize;
    public int yOffset;
    public float variance;
    
    // Start is called before the first frame update
    void Start()
    {
        int cubeCount = 200 / cubeSize;
        for (int i = 0; i < cubeCount; i++)
        {
            for (int j = 0; j < cubeCount; j++)
            {
                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                cube.transform.position = new Vector3(-100 + (i * cubeSize), yOffset, -100 + (j * cubeSize));
                cube.transform.localScale = new Vector3(cubeSize, cubeSize + Random.Range(0.0f, variance), cubeSize);
                Material instance = Instantiate(cubeMaterial);
                Color chosen = cubeColours[Random.Range(0, cubeColours.Length)];
                instance.SetColor("_Color", chosen);
                instance.SetColor("_EmissiveColor", chosen);
                cube.GetComponent<Renderer>().material = instance;
            }
        }
    }
}
