using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LocationDisplayUpdater : MonoBehaviour
{
    public TextMeshPro latitudeText;
    public TextMeshPro longitudeText;
    public TextMeshPro depthText;

    private UserPosition calculator;

    // Update is called once per frame
    void Update()
    {
        if (calculator.displayedSystem == UserPosition.CoordinateSystem.NZGD2000)
        {
            latitudeText.text = $"{calculator.latitude} S";
            longitudeText.text = $"{calculator.longitude} E";
        } 
        else
        {
            latitudeText.text = $"{calculator.latitude} N";
            longitudeText.text = $"{calculator.longitude} E";
        }
        depthText.text = $"{calculator.depth} m";
    }

    public void registerUserPosition(UserPosition instance)
    {
        calculator = instance;
    }
}
