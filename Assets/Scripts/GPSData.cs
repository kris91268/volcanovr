using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GPSData
{
    private GPSDataEntry[] east, north, up;
    private int currentIndex;

    public GPSData(List<string> eastLines, List<string> northLines, List<string> upLines)
    {
        east = new GPSDataEntry[eastLines.Count];
        north = new GPSDataEntry[northLines.Count];
        up = new GPSDataEntry[upLines.Count];
        int index = 0;
        foreach (string line in eastLines)
        {
            east[index++] = parseLine(line);
        }
        index = 0;
        foreach (string line in northLines)
        {
            north[index++] = parseLine(line);
        }
        index = 0;
        foreach (string line in upLines)
        {
            up[index++] = parseLine(line);
        }
    }

    public GPSDataEntry[] nextValue()
    {
        currentIndex++;
        if (currentIndex >= east.Length || currentIndex >= north.Length || currentIndex >= up.Length)
        {
            currentIndex = 0;
        }
        return getCurrentValues();
    }

    public GPSDataEntry[] getCurrentValues()
    {
        var entries = new GPSDataEntry[3];
        entries[0] = east[currentIndex];
        entries[1] = north[currentIndex];
        entries[2] = up[currentIndex];
        return entries;
    }

    private static DateTime parseUTCDateTimeString(string utc)
    {
        string[] parts = utc.Split('T');
        string[] dates = parts[0].Split('-');
        int year = int.Parse(dates[0]);
        int month = int.Parse(dates[1]);
        int day = int.Parse(dates[2]);
        string[] time = parts[1].Split('.')[0].Split(':');
        int hour = int.Parse(time[0]);
        int minute = int.Parse(time[1]);
        int second = int.Parse(time[2]);
        return new DateTime(year, month, day, hour, minute, second);
    }

    private static GPSDataEntry parseLine(string line)
    {
        string[] parts = line.Split(',');
        DateTime time = parseUTCDateTimeString(parts[0]);
        float value = float.Parse(parts[1]);
        float error = float.Parse(parts[2]);
        return new GPSDataEntry(time, value, error);
    }

    public readonly struct GPSDataEntry
    {
        public GPSDataEntry(DateTime t, float val, float err)
        {
            time = t;
            value = val;
            error = err;
        }
        public DateTime time { get; }
        public float value { get; }
        public float error { get; }
    }
}
