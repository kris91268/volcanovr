using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SpatialTracking;
using System;
using Valve.VR;

public class ControllerSelector : MonoBehaviour
{
    public GameObject leftHand, rightHand;
    public GameObject mainCamera;

    // Start is called before the first frame update
    void Start()
    {
        if (SteamVR.enabled)
        {
            Debug.Log("VR system detected");
            return;
        }
        Debug.LogError("No VR System detected");
        Destroy(GetComponent<SteamVR_PlayArea>());
        Destroy(leftHand);
        Destroy(rightHand);
        mainCamera.transform.position = new Vector3(0, 0.5f, 0);
        Destroy(mainCamera.GetComponent<SteamVR_CameraHelper>());
        
        GameObject.Find("MainCanvas").GetComponent<Canvas>().worldCamera = mainCamera.GetComponent<Camera>();
        GameObject.Find("PresetsCanvas").GetComponent<Canvas>().worldCamera = mainCamera.GetComponent<Camera>();

        Destroy(GameObject.Find("EventSystem").GetComponent<VRInput>());
        
        Destroy(this);
    }
}
