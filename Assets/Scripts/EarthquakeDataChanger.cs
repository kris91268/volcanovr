using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EarthquakeDataChanger : MonoBehaviour
{
    public enum EarthquakeDataType
    {
        MAGNITUDE,
        TIME
    }

    private List<DataHandler.Earthquake> quakes;
    private bool registered = false;
    private float largestMagnitude, smallestMagnitude;

    public Gradient earthquakeMagnitudeGradient;
    public Gradient earthquakeTimeGradient;

    private EarthquakeDataType currentDisplay;


    public void addEarthquakes(List<DataHandler.Earthquake> quakes)
    {
        this.quakes = quakes;
        registered = true;
        float smallest = float.MaxValue;
        float largest = 0.0f;
        foreach (DataHandler.Earthquake quake in quakes)
        {
            if (quake.magnitude > largest)
            {
                largest = quake.magnitude;
            }
            if (quake.magnitude < smallest)
            {
                smallest = quake.magnitude;
            }
        }
        largestMagnitude = largest;
        smallestMagnitude = smallest;
    }

    public void changeEarthquakeDataDisplay(EarthquakeDataType to)
    {
        currentDisplay = to;
        if (!registered)
        {
            Debug.Log("No earthquakes registered");
            return;
        }
        switch (to)
        {
            case EarthquakeDataType.MAGNITUDE:
                foreach (var quake in quakes)
                {
                    Renderer r = quake.gameObject.GetComponent<Renderer>();
                    Color color = earthquakeMagnitudeGradient.Evaluate((quake.magnitude - smallestMagnitude) / (largestMagnitude - smallestMagnitude));
                    r.material.SetColor("_Color", color);
                    r.material.SetColor("_EmissionColor", color);
                }
                break;
            case EarthquakeDataType.TIME:
                DateTime first = parseUTCDateTimeString(quakes[0].time);
                DateTime last = parseUTCDateTimeString(quakes[quakes.Count - 1].time);
                double delta = last.Subtract(first).TotalSeconds;
                foreach (var quake in quakes)
                {
                    Renderer r = quake.gameObject.GetComponent<Renderer>();
                    double currentDelta = last.Subtract(parseUTCDateTimeString(quake.time)).TotalSeconds;
                    Color c = earthquakeTimeGradient.Evaluate((float)(currentDelta / delta));
                    r.material.SetColor("_Color", c);
                    r.material.SetColor("_EmissionColor", c);
                }
                break;
        }
    }

    public EarthquakeDataType getCurrentType()
    {
        return this.currentDisplay;
    }

    private DateTime parseUTCDateTimeString(string utc)
    {
        string[] parts = utc.Split('T');
        string[] dates = parts[0].Split('-');
        int year = int.Parse(dates[0]);
        int month = int.Parse(dates[1]);
        int day = int.Parse(dates[2]);
        string[] time = parts[1].Split('.')[0].Split(':');
        int hour = int.Parse(time[0]);
        int minute = int.Parse(time[1]);
        int second = int.Parse(time[2]);
        return new DateTime(year, month, day, hour, minute, second);
    }
}