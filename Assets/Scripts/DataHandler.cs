using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Rendering;
using DotnetDataLens;
using System.IO;

public class DataHandler : MonoBehaviour
{
    public readonly struct Earthquake
    {
        public Earthquake(GameObject obj, string date, double lo, double lt, int d, float m)
        {
            gameObject = obj;
            time = date;
            longitude = lo;
            latitude = lt;
            depth = d;
            magnitude = m;
        }
        public GameObject gameObject { get; }
        public string time { get; }
        public double longitude { get; }
        public double latitude { get; }
        public int depth { get; }
        public float magnitude { get; }

        public override string ToString()
        {
            return $"Earthquake (M{magnitude}) at lat: {latitude} and long: {longitude} at depth: {depth}m. Occured at {time}";
        }
    }

    [HideInInspector]
    public List<Earthquake> currentQuakes = new List<Earthquake>();
    [HideInInspector]
    public Material earthquakeMaterial;
    public GameObject labelPrefab;
    public GameObject playerCamera;

    private double originLatitude, originLongitude;
    private int width;

    private Datalens datalens;

    void Start()
    {
        originLatitude = double.Parse(PlayerPrefs.GetString("latitude"));
        originLongitude = double.Parse(PlayerPrefs.GetString("longitude"));
        width = PlayerPrefs.GetInt("width");
        datalens = new Datalens();
    }

    public void loadData()
    {
        string preset = PlayerPrefs.GetString("preset");
        int data = PlayerPrefs.GetInt("data"); 
        loadFitsStations();
        if ((data & (1 << 1)) == 1)
        {
            
        }
        if ((data & 1) == 1)
        {
            
        }
        if (preset.Equals("none"))
        {
            loadGeonetData();
        }
        else
        {
            loadPresetData();
        }
    }

    private static double[] getLatLongBoundaries(double originLat, double originLong, int widthInKm)
    {
        double[] bounds = new double[4];
        double[] nztm = CoordinateConverter.geographic_to_transverse_mercator(originLat, originLong);
        int halfWidth = (widthInKm * 1000) / 2;
        int startEasting = (int)nztm[0] - halfWidth;
        int endEasting = (int)nztm[0] + halfWidth;
        int startNorthing = (int)nztm[1] - halfWidth;
        int endNorthing = (int)nztm[1] + halfWidth;
        double[] start = CoordinateConverter.transverse_mercator_to_geographic(startEasting, startNorthing);
        double[] end = CoordinateConverter.transverse_mercator_to_geographic(endEasting, endNorthing);
        bounds[0] = end[1];
        bounds[1] = start[0];
        bounds[2] = start[1];
        bounds[3] = end[0];
        return bounds;
    }

    private void loadFitsStations()
    {
        double[] boundaries = getLatLongBoundaries(originLatitude, originLongitude, width);
        List<Fits.FitsSite> sites = datalens.fits.getSitesInBounds(boundaries[0], boundaries[1], boundaries[2], boundaries[3]);
        double[] originNZTM = CoordinateConverter.geographic_to_transverse_mercator(originLatitude, originLongitude);
        int startEasting = (int)originNZTM[0] - ((width / 2) * 1000);
        int startNorthing = (int)originNZTM[1] + ((width / 2) * 1000);
        Vector3 pos = this.GetComponent<Terrain>().GetPosition();
        double ratio = (width * 1000) / this.GetComponent<Terrain>().terrainData.size.x;
        foreach (var site in sites)
        {
            double[] nztm = CoordinateConverter.geographic_to_transverse_mercator(site.latitude, site.longitude);
            GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            obj.name = $"FITS: {site.siteID}";
            int deltaEasting = Math.Abs((int)nztm[0] - startEasting);
            int deltaNorthing = Math.Abs((int)nztm[1] - startNorthing);
            float z = pos.z + (deltaEasting / (float)ratio);
            float x = pos.x + (deltaNorthing / (float)ratio);
            float y = 0.0f;
            RaycastHit hit;
            Ray ray = new Ray(new Vector3(x, 1000.0f, z), new Vector3(0.0f, -1.0f, 0.0f));
            Physics.Raycast(ray, out hit);
            if (hit.collider)
            {
                y = hit.point.y;
            }
            obj.transform.position = new Vector3(x, y, z);
            obj.transform.localScale = new Vector3(2.0f, 2.0f, 2.0f);
            Renderer r = obj.GetComponent<Renderer>();
            r.receiveShadows = false;
            r.shadowCastingMode = ShadowCastingMode.Off;
            r.material = Instantiate(earthquakeMaterial);
            r.material.SetColor("_Color", new Color(0.1f, 0.95f, 0.1f));
            // fitsSites.Add(new FitsSite(obj, site.siteID, site.latitude, site.longitude, site.height, site.name));
            GameObject label = Instantiate(labelPrefab, obj.transform);
            label.GetComponent<SiteLabelBehaviour>().text = site.siteID;
            label.name = $"{site.siteID} Label";
            label.GetComponent<SiteLabelBehaviour>().player = playerCamera;
            label.transform.localPosition += new Vector3(0.0f, 5.0f, 0.0f);
        }
    }

    private void loadGeonetData()
    {
        double[] bounds = getLatLongBoundaries(originLatitude, originLongitude, width);
        List<Fdsn.FdsnEvent> quakes = datalens.fdsn.getEventsWithinTimeAndBounds(parseDateFromInput(PlayerPrefs.GetString("from_date")), 
            parseDateFromInput(PlayerPrefs.GetString("to_date")), bounds[2], bounds[1], bounds[0], bounds[3]);
        double[] originNZTM = CoordinateConverter.geographic_to_transverse_mercator(originLatitude, originLongitude);
        int startEasting = (int)originNZTM[0] - ((width / 2) * 1000);
        int startNorthing = (int)originNZTM[1] + ((width / 2) * 1000);
        Vector3 pos = this.GetComponent<Terrain>().GetPosition();   // is at 0, 0, 0 anyway but incase this changes, this is included here
        double ratio = (width * 1000) / this.GetComponent<Terrain>().terrainData.size.x; // turns out ratio between meters and unity units is in fact the width\
        float heightRatio = (this.GetComponent<Terrain>().terrainData.size.x / (width * 1000f));
        foreach (var quake in quakes)
        {
            GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            obj.name = "Earthquake";
            currentQuakes.Add(new Earthquake(obj, quake.time, quake.longitude, quake.latitude, (int)(quake.depth * 1000), quake.magnitude));
            double[] nztm = CoordinateConverter.geographic_to_transverse_mercator(quake.latitude, quake.longitude);
            int deltaEasting = Math.Abs((int)nztm[0] - startEasting);
            int deltaNorthing = Math.Abs((int)nztm[1] - startNorthing);
            float z = pos.z + (deltaEasting / (float)ratio);
            float x = pos.x + (deltaNorthing / (float)ratio);
            float y = pos.y - (float)(quake.depth * 1000.0f * heightRatio);
            obj.transform.position = new Vector3(x, y, z);
            obj.transform.localScale = new Vector3(2.0f, 2.0f, 2.0f);
            Renderer r = obj.GetComponent<Renderer>();
            r.receiveShadows = false;
            r.shadowCastingMode = ShadowCastingMode.Off;
            r.material = Instantiate(earthquakeMaterial);
        }
        this.GetComponent<EarthquakeDataChanger>().addEarthquakes(currentQuakes);
        this.GetComponent<EarthquakeDataChanger>().changeEarthquakeDataDisplay(EarthquakeDataChanger.EarthquakeDataType.MAGNITUDE);
    }

    private void loadPresetData()
    {
        string preset = PlayerPrefs.GetString("preset");
        string[] lines = File.ReadAllLines(preset);
        double[] originNZTM = CoordinateConverter.geographic_to_transverse_mercator(originLatitude, originLongitude);
        int startEasting = (int)originNZTM[0] - ((width / 2) * 1000);
        int startNorthing = (int)originNZTM[1] + ((width / 2) * 1000);
        Vector3 pos = this.GetComponent<Terrain>().GetPosition();
        double ratio = (width * 1000) / this.GetComponent<Terrain>().terrainData.size.x;
        float heightRatio = (this.GetComponent<Terrain>().terrainData.size.x / (width * 1000f));
        for (int i = 6; i < lines.Length; i++)
        {
            if (lines[i] == "")
            {
                break;
            }
            try
            {
                string[] data = lines[i].Split(',');
                GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                obj.name = "Earthquake";
                string time = data[0];
                double longitude = double.Parse(data[1]);
                double latitude = double.Parse(data[2]);
                int depth = (int)float.Parse(data[3]);
                float magnitude = float.Parse(data[4]);
                currentQuakes.Add(new Earthquake(obj, time, longitude, latitude, depth, magnitude));
                double[] nztm = CoordinateConverter.geographic_to_transverse_mercator(latitude, longitude);
                int deltaEasting = Math.Abs((int)nztm[0] - startEasting);
                int deltaNorthing = Math.Abs((int)nztm[1] - startNorthing);
                float z = pos.z + (deltaEasting / (float)ratio);
                float x = pos.x + (deltaNorthing / (float)ratio);
                float y = pos.y - (depth * heightRatio);
                obj.transform.position = new Vector3(x, y, z);
                obj.transform.localScale = new Vector3(2.0f, 2.0f, 2.0f);
                Renderer r = obj.GetComponent<Renderer>();
                r.receiveShadows = false;
                r.shadowCastingMode = ShadowCastingMode.Off;
                r.material = Instantiate(earthquakeMaterial);
            }
            #pragma warning disable 168
            catch (System.Exception exc)
            {
                Debug.Log($"Invalid data entry at line {i}. Please check and rectify");
            }
            #pragma warning restore 168
        }
        Debug.Log($"{currentQuakes.Count} quakes loaded");
        this.GetComponent<EarthquakeDataChanger>().addEarthquakes(currentQuakes);
        this.GetComponent<EarthquakeDataChanger>().changeEarthquakeDataDisplay(EarthquakeDataChanger.EarthquakeDataType.MAGNITUDE);
    }

    private static string parseDateFromInput(string input)
    {
        string[] parts = input.Split('/');
        return $"{parts[2]}-{parts[1]}-{parts[0]}T12:00:00.000";
    }
}
