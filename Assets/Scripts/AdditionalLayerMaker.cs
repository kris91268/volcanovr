using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DotnetDataLens;

public class AdditionalLayerMaker : MonoBehaviour
{
    public enum Overlays
    {
        SATELLITE,
        ELEVATION,
        GEOLOGY_MAP,
        GRAVITY
    }

    [HideInInspector]
    public Overlays currentOverlay = Overlays.SATELLITE;
    public Gradient elevationGradient;

    [HideInInspector]
    public Map map;

    public void addElevationOverlay()
    {
        Terrain terrain = this.GetComponent<Terrain>();
        Texture2D overlay = new Texture2D(terrain.terrainData.heightmapTexture.width, terrain.terrainData.heightmapTexture.height);
        TerrainLayer[] layers = new TerrainLayer[terrain.terrainData.terrainLayers.Length + 1];
        layers[0] = terrain.terrainData.terrainLayers[(int)Overlays.SATELLITE];
        float heightMax = 0.0f;
        float heightMin = float.MaxValue;
        for (int i = 0; i < terrain.terrainData.heightmapTexture.width; i++)
        {
            for (int j = 0; j < terrain.terrainData.heightmapTexture.height; j++)
            {
                float current = terrain.terrainData.GetHeight(i, j);
                if (heightMax < current)
                {
                    heightMax = current;
                }
                if (heightMin > current)
                {
                    heightMin = current;
                }
            }
        }
        for (int i = 0; i < terrain.terrainData.heightmapTexture.width; i++)
        {
            for (int j = 0; j < terrain.terrainData.heightmapTexture.height; j++)
            {
                float heightNormalized = (terrain.terrainData.GetHeight(i, j) - heightMin) / (heightMax - heightMin);
                overlay.SetPixel(i, j, elevationGradient.Evaluate(heightNormalized));
            }
        }
        overlay.Apply();
        layers[(int)Overlays.ELEVATION] = new TerrainLayer();
        layers[(int)Overlays.ELEVATION].diffuseTexture = overlay;
        layers[(int)Overlays.ELEVATION].tileOffset = new Vector2(0, 0);
        layers[(int)Overlays.ELEVATION].tileSize = new Vector2(1000, 1000);
        layers[(int)Overlays.ELEVATION].smoothness = 0.0f;
        terrain.terrainData.terrainLayers = layers;
    }

    public void addGeologyMapOverlay()
    {
        double[] nztm = CoordinateConverter.geographic_to_transverse_mercator(double.Parse(PlayerPrefs.GetString("latitude")), 
            double.Parse(PlayerPrefs.GetString("longitude")));
        int width = PlayerPrefs.GetInt("width");
        int west = (int)nztm[0] - ((width * 1000) / 2);
        int north = (int)nztm[1] + ((width * 1000) / 2);
        Datalens lens = new Datalens();
        Map map = lens.createMap(west, north, north - (width * 1000), west + (width * 1000));
        Texture2D geologyMap = new Texture2D(map.getSize(), map.getSize());
        geologyMap.LoadImage(map.geologyMap);
        Texture2D faultMap = new Texture2D(map.getSize(), map.getSize());
        faultMap.LoadImage(map.faultMap);
        for (int i = 0; i < geologyMap.width; i++)
        {
            for (int j = 0; j < geologyMap.height; j++)
            {
                Color c = faultMap.GetPixel(i, j);
                if (c != Color.white)
                {
                    geologyMap.SetPixel(i, j, c);
                }
            }
        }
        geologyMap.Apply();
        geologyMap = TerrainGenerator.rotateTexture(geologyMap, false);
        Terrain terrain = GetComponent<Terrain>();
        TerrainLayer[] layers = new TerrainLayer[terrain.terrainData.terrainLayers.Length + 1];
        layers[0] = terrain.terrainData.terrainLayers[(int)Overlays.SATELLITE];
        layers[1] = terrain.terrainData.terrainLayers[(int)Overlays.ELEVATION];
        layers[(int)Overlays.GEOLOGY_MAP] = new TerrainLayer();
        layers[(int)Overlays.GEOLOGY_MAP].diffuseTexture = geologyMap;
        layers[(int)Overlays.GEOLOGY_MAP].tileOffset = new Vector2(0, 0);
        layers[(int)Overlays.GEOLOGY_MAP].tileSize = new Vector2(1000, 1000);
        layers[(int)Overlays.GEOLOGY_MAP].smoothness = 0.0f;
        terrain.terrainData.terrainLayers = layers;
        this.map = map;
    }

    public void displayNextOverlay()
    {
        Overlays[] list = new Overlays[] { Overlays.SATELLITE, Overlays.ELEVATION, Overlays.GEOLOGY_MAP };
        int currentIndex = 0;
        for (int i = 0; i < list.Length; i++)
        {
            if (list[i] == currentOverlay)
            {
                break;
            }
            currentIndex++;
        }
        if (currentIndex == list.Length - 1)
        {
            switchOverlay(list[0]);
        }
        else
        {
            switchOverlay(list[currentIndex + 1]);
        }
    }

    public void switchOverlay(Overlays to)
    {
        TerrainData data = this.GetComponent<Terrain>().terrainData;
        float[,,] alpha = new float[data.alphamapResolution, data.alphamapResolution, data.terrainLayers.Length];
        for (int i = 0; i < alpha.GetLength(0); i++)
        {
            for (int j = 0; j < alpha.GetLength(1); j++)
            {
                for (int k = 0; k < alpha.GetLength(2); k++)
                {
                    if (k == (int)to)
                    {
                        alpha[i, j, k] = 1.0f;
                    }
                    else
                    {
                        alpha[i, j, k] = 0.0f;
                    }
                }
            }
        }
        currentOverlay = to;
        data.SetAlphamaps(0, 0, alpha);
    }
}
