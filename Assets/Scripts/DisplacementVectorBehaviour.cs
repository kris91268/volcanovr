using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplacementVectorBehaviour : MonoBehaviour
{
    private float magnitudeX, magnitudeY, magnitudeZ;
    public float factor = 100.0f; // how many units of mm displacement length per unit unity distance?

    private const float headOffset = 0.5f;

    public GameObject xArrow, yArrow, zArrow;
    public GameObject xArrowHead, yArrowHead, zArrowHead;

    // Start is called before the first frame update
    void Start()
    {
        setNewMagnitudes(100.0f, 100.0f, 100.0f);
    }

    private void updateArrows()
    {
        float scaleX = magnitudeX / factor;
        float scaleY = magnitudeY / factor;
        float scaleZ = magnitudeZ / factor;
        xArrow.transform.localScale = new Vector3(scaleX, 1.0f, 1.0f);
        yArrow.transform.localScale = new Vector3(1.0f, scaleY, 1.0f);
        zArrow.transform.localScale = new Vector3(1.0f, 1.0f, scaleZ);
        if (scaleX < 0.0f)
        {
            xArrow.transform.position = new Vector3(-(scaleX / 2) - 0.5f, 0.0f, 0.0f);
            xArrowHead.transform.position = new Vector3(-scaleX - headOffset, 0.0f, 0.0f);
        }
        else
        {
            xArrow.transform.position = new Vector3(scaleX / 2 + 0.5f, 0.0f, 0.0f);
            xArrowHead.transform.position = new Vector3(scaleX + headOffset, 0.0f, 0.0f);
        }
        if (scaleY < 0.0f)
        {
            yArrow.transform.position = new Vector3(0.0f, -(scaleY / 2) - 0.5f, 0.0f);
            yArrowHead.transform.position = new Vector3(0.0f, -scaleY - headOffset, 0.0f);
        }
        else
        {
            yArrow.transform.position = new Vector3(0.0f, (scaleY / 2) + 0.5f, 0.0f);
            yArrowHead.transform.position = new Vector3(0.0f, scaleY + headOffset, 0.0f);
        }
        if (scaleZ < 0.0f)
        {
            zArrow.transform.position = new Vector3(0.0f, 0.0f, -(scaleZ / 2) - 0.5f);
            zArrowHead.transform.position = new Vector3(0.0f, 0.0f, -scaleZ - headOffset);
        } 
        else
        {
            zArrow.transform.position = new Vector3(0.0f, 0.0f, (scaleZ / 2) + 0.5f);
            zArrowHead.transform.position = new Vector3(0.0f, 0.0f, scaleZ + headOffset);
        }
    }

    public void setNewMagnitudes(float x, float y, float z)
    {
        magnitudeX = x;
        magnitudeY = y;
        magnitudeZ = z;
        updateArrows();
    }
}
