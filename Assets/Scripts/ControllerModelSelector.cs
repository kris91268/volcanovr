using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using UnityEngine.Rendering;

public class ControllerModelSelector : MonoBehaviour
{
    public GameObject leftController, rightController;

    public GameObject leftOculusModel;
    public GameObject rightOculusModel;

    public GameObject viveModel;

    private void Start()
    {
        string model = PlayerPrefs.GetString("platform");
        Debug.Log($"Connected with device {model}");
        if (model.Equals("oculus"))
        {
            GameObject lc = Instantiate(leftOculusModel, leftController.transform);
            GameObject rc = Instantiate(rightOculusModel, rightController.transform);
            lc.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            rc.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            var lcr = lc.GetComponent<Renderer>();
            var rcr = rc.GetComponent<Renderer>();
            lcr.receiveShadows = false;
            rcr.receiveShadows = false;
            lcr.shadowCastingMode = ShadowCastingMode.Off;
            rcr.shadowCastingMode = ShadowCastingMode.Off;
        }
        else if (model.Equals("vive"))
        {
            GameObject lc = Instantiate(viveModel, leftController.transform);
            GameObject rc = Instantiate(viveModel, rightController.transform);
            lc.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            rc.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            lc.transform.Rotate(0.0f, 180.0f, 0.0f);
            rc.transform.Rotate(0.0f, 180.0f, 0.0f);
            var lcr = lc.GetComponent<Renderer>();
            var rcr = rc.GetComponent<Renderer>();
            lcr.receiveShadows = false;
            rcr.receiveShadows = false;
            lcr.shadowCastingMode = ShadowCastingMode.Off;
            rcr.shadowCastingMode = ShadowCastingMode.Off;
        }
        else
        {
            Debug.Log("Platform custom controller not supported");
        }
    }
}
