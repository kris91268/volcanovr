using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class GraphTextMaker
{
    public enum Alignment
    {
        LEFT,
        CENTER,
        RIGHT
    }

    public struct CharCoords
    {
        public CharCoords(char c, Vector2Int tlc, Vector2Int brc)
        {
            character = c;
            topLeftCorner = tlc;
            bottomRightCorner = brc;
        }
        public char character { get; private set; }
        public Vector2Int topLeftCorner { get; private set; }
        public Vector2Int bottomRightCorner { get; private set; }
    }

    private List<CharCoords> characterMap;
    private Texture2D fontTexture;

    private Texture2D canvas;

    private const int UNIVERSAL_CHARACTER_HEIGHT = 22;

    public GraphTextMaker(Texture2D canvas)
    {
        TextAsset map = Resources.Load("calibri_coordinate_map") as TextAsset;
        characterMap = new List<CharCoords>();
        string[] lines = map.text.Split('\n');
        foreach (string line in lines)
        {
            char c = line.ToCharArray()[0];
            string[] numbers = line.Substring(2).Split(',');
            int startX = int.Parse(numbers[0]);
            int startY = int.Parse(numbers[1]);
            int endX = int.Parse(numbers[2]);
            int endY = int.Parse(numbers[3]);
            characterMap.Add(new CharCoords(c, new Vector2Int(startX, startY), new Vector2Int(endX, endY)));
        }
        fontTexture = Resources.Load("calibri_font_characters") as Texture2D;
        fontTexture.Apply();
        this.canvas = canvas;
    }

    private CharCoords getBoundsOfCharacter(char c)
    {
        foreach (var coordinate in characterMap)
        {
            if (coordinate.character == c)
            {
                return coordinate;
            }
        }
        throw new ArgumentException("Character not found");
    }

    public void drawText(string text, Alignment alignment, Vector2Int position, float scale)
    {
        char[] characters = text.ToCharArray();
        List<CharCoords> textInCoords = new List<CharCoords>();
        int totalWidth = 0;
        foreach (char c in characters)
        {
            var coord = getBoundsOfCharacter(c);
            textInCoords.Add(coord);
            totalWidth += coord.bottomRightCorner.x - coord.topLeftCorner.x;
        }
        totalWidth = (int)(totalWidth * scale);
        int height = (int)(UNIVERSAL_CHARACTER_HEIGHT * scale);
        int currentX = position.x;
        int currentY = position.y;
        switch (alignment)
        {
            case Alignment.LEFT:
                currentX = position.x;
                break;
            case Alignment.CENTER:
                currentX = position.x - (totalWidth / 2);
                break;
            case Alignment.RIGHT:
                currentX = position.x - totalWidth;
                break;
        }
        foreach (var symbol in textInCoords)
        {
            Texture2D tex = scaleCharacter(scale, symbol);
            for (int i = 0; i < tex.width; i++)
            {
                for (int j = 0; j < tex.height; j++)
                {
                    canvas.SetPixel(currentX + i, currentY + j, tex.GetPixel(i, tex.height - j));
                }
            }
            currentX += tex.width;
        }
        canvas.Apply();
    }

    private Texture2D scaleCharacter(float scale, CharCoords character)
    {
        int w = character.bottomRightCorner.x - character.topLeftCorner.x;
        int h = character.bottomRightCorner.y - character.topLeftCorner.y;
        Texture2D tex = new Texture2D(w, h);
        for (int i = 0; i < w; i++)
        {
            for (int j = 0; j < h; j++)
            {
                tex.SetPixel(i, j, fontTexture.GetPixel(character.topLeftCorner.x + i, fontTexture.height - (character.topLeftCorner.y + j)));
            }
        }
        tex.Apply();
        TextureScaler.Point(tex, (int)(w * scale), (int)(h * scale));
        // File.WriteAllBytes($"output/{character.character}.png", tex.EncodeToPNG());
        return tex;
    }
}
