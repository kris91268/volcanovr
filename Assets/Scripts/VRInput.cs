using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR;
using UnityEngine.UI;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class VRInput : BaseInputModule
{
    public Camera myCamera; // camera already exists in base class

    private GameObject currentObject = null;
    private PointerEventData pointerData = null;

    public GameObject keyboardPrefab;
    public GameObject mainCamera;

    private GameObject vrKeyboardInstance = null;

    public SteamVR_ActionSet actionSet;
    public SteamVR_Action_Boolean interact;
    public SteamVR_Input_Sources hand;


    protected override void Awake()
    {
        base.Awake();
        pointerData = new PointerEventData(eventSystem);
    }

    protected override void Start()
    {
        base.Start();
    }

    public override void Process()
    {
        pointerData.Reset();
        pointerData.position = new Vector2(myCamera.pixelWidth / 2, myCamera.pixelHeight / 2);
        eventSystem.RaycastAll(pointerData, m_RaycastResultCache);
        pointerData.pointerCurrentRaycast = FindFirstRaycast(m_RaycastResultCache);
        currentObject = pointerData.pointerCurrentRaycast.gameObject;
        m_RaycastResultCache.Clear();
        HandlePointerExitAndEnter(pointerData, currentObject);
        if (interact.GetStateDown(hand))
        {
            processPress(pointerData);
        }
        if (interact.GetStateUp(hand))
        {
            processRelease(pointerData);
        }
    }

    public PointerEventData getData()
    {
        return this.pointerData;
    }

    private void processPress(PointerEventData data)
    {
        data.pointerPressRaycast = data.pointerCurrentRaycast;
        GameObject newPress = ExecuteEvents.ExecuteHierarchy(currentObject, data, ExecuteEvents.pointerDownHandler);
        if (newPress ==  null)
        {
            newPress = ExecuteEvents.GetEventHandler<IPointerClickHandler>(currentObject);
        }
        data.pressPosition = data.position;
        data.pointerPress = newPress;
        data.rawPointerPress = currentObject;
    }

    private void processRelease(PointerEventData data)
    {
        ExecuteEvents.Execute(data.pointerPress, data, ExecuteEvents.pointerUpHandler);
        GameObject pointerUpHandler = ExecuteEvents.GetEventHandler<IPointerClickHandler>(currentObject);
        if (data.pointerPress == pointerUpHandler)
        {
            ExecuteEvents.Execute(data.pointerPress, data, ExecuteEvents.pointerClickHandler);
        }
        InputField field = data.pointerPress.GetComponent<InputField>();
        if (field != null)
        {
            if (vrKeyboardInstance != null)
            {
                Destroy(vrKeyboardInstance);
            }
            GameObject keyboard = Instantiate(keyboardPrefab);
            keyboard.transform.position = mainCamera.transform.position + (mainCamera.transform.forward * 1.5f);
            keyboard.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            keyboard.name = "VR Number Keyboard";
            var handler = keyboard.GetComponent<VRNumberKeyboardHandler>();
            handler.eventCamera = myCamera;
            handler.vrInput = this;
            handler.currentInputField = field;
            vrKeyboardInstance = keyboard;
        }
        eventSystem.SetSelectedGameObject(null);
        data.pressPosition = Vector2.zero;
        data.pointerPress = null;
        data.rawPointerPress = null;
    }

    public GameObject getCurrentObject()
    {
        return this.gameObject;
    }
}
