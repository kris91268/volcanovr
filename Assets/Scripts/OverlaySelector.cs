using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using System;

public class OverlaySelector : MonoBehaviour
{
    [HideInInspector]
    public AdditionalLayerMaker.Overlays currentOverlay;
    private AdditionalLayerMaker.Overlays previousOverlay;

    public SteamVR_Action_Vector2 joystick;
    public SteamVR_Input_Sources hand;

    public GameObject topLeft, topRight, bottomRight, bottomLeft;
    public Color selectionColor;
    public Color normalColor;


    // Update is called once per frame
    void Update()
    {
        Vector2 axes = joystick.GetAxis(hand);
        if (axes.x == 0 && axes.y == 0)
        {
            deselectAll();
            return;
        }
        float degrees = Mathf.Rad2Deg * Mathf.Atan2(axes.y, axes.x);
        if (inRange(degrees, 0.0f, 90.0f))
        {
            currentOverlay = AdditionalLayerMaker.Overlays.SATELLITE;
            changeColor(topRight, new GameObject[] { topLeft, bottomLeft, bottomRight });
        }
        else if (inRange(degrees, -90.0f, 0.0f))
        {
            changeColor(bottomRight, new GameObject[] { topLeft, topRight, bottomLeft });
        }
        else if (inRange(degrees, -180.0f, -90.0f))
        {
            currentOverlay = AdditionalLayerMaker.Overlays.GEOLOGY_MAP;
            changeColor(bottomLeft, new GameObject[] { topLeft, topRight, bottomRight });
        }
        else if (inRange(degrees, 90.0f, 180.0f))
        {
            currentOverlay = AdditionalLayerMaker.Overlays.ELEVATION;
            changeColor(topLeft, new GameObject[] { topRight, bottomLeft, bottomRight });
        }
        previousOverlay = currentOverlay;
    }

    private void changeColor(GameObject selection, GameObject[] others)
    {
        selection.GetComponent<Renderer>().material.SetColor("_Color", selectionColor);
        foreach (GameObject o in others)
        {
            o.GetComponent<Renderer>().material.SetColor("_Color", normalColor);
        }
    }

    private void deselectAll()
    {
        topLeft.GetComponent<Renderer>().material.SetColor("_Color", normalColor);
        topRight.GetComponent<Renderer>().material.SetColor("_Color", normalColor);
        bottomLeft.GetComponent<Renderer>().material.SetColor("_Color", normalColor);
        bottomRight.GetComponent<Renderer>().material.SetColor("_Color", normalColor);
    }

    private static bool inRange(float val, float first, float second)
    {
        return first <= val && val <= second;
    }
}
