using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRVisualizationLaserPointer : VRLaserPointer
{
    private GameObject pointingAt;
    private RaycastHit hit;

    // Update is called once per frame
    void Update()
    {
        lineRenderer.SetPosition(0, transform.position);
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.forward);
        Physics.Raycast(ray, out hit);
        Vector3 end = transform.position + (transform.forward * defaultLength);
        if (hit.collider)
        {
            end = hit.point;
            pointingAt = hit.collider.gameObject;
        }
        lineRenderer.SetPosition(1, end);
        this.hit = hit;
    }

    public string getNameOfGameObjectPointingAt()
    {
        return pointingAt.name;
    }

    public GameObject getObjectPointingAt()
    {
        return pointingAt;
    }

    public RaycastHit getRaycastHit()
    {
        return hit;
    }
}
