using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EarthquakeDetailsPrefabController : MonoBehaviour
{
    public Text latitudeText;
    public Text longitudeText;
    public Text timeText;
    public Text depthText;
    public Text magnitudeText;

    public Canvas parentCanvas;

    public void registerCamera(Camera camera)
    {
        parentCanvas.worldCamera = camera;
    }

    public void setLatitude(double latitude)
    {
        latitudeText.text = $"{latitude} S";
    }

    public void setLongitude(double longitude)
    {
        longitudeText.text = $"{longitude} E";
    }

    public void setTime(string time)
    {
        timeText.text = time;
    }

    public void setDepth(int depth)
    {
        depthText.text = $"{depth} m";
    }

    public void setMagnitude(float magnitude)
    {
        magnitudeText.text = $"M{magnitude}";
    }

    public void setAll(double lat, double ln, string time, int depth, float mag)
    {
        setLatitude(lat);
        setLongitude(ln);
        setTime(time);
        setDepth(depth);
        setMagnitude(mag);
    }

    public void closeWindow()
    {
        Destroy(this.gameObject);
    }
}
