using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectConfigurations : MonoBehaviour
{
    public enum VRPlatform
    {
        OCULUS_QUEST,
        HTC_VIVE
    }

    public VRPlatform currentPlatform;

    private const string PLATFORM_IDENTIFIER = "platform";

    private void Awake()
    {
        switch (currentPlatform)
        {
            case VRPlatform.OCULUS_QUEST:
                PlayerPrefs.SetString(PLATFORM_IDENTIFIER, "oculus");
                break;
            case VRPlatform.HTC_VIVE:
                PlayerPrefs.SetString(PLATFORM_IDENTIFIER, "vive");
                break;
        }
    }
}
