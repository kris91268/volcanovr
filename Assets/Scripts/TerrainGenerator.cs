using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;
using System.Threading;

public class TerrainGenerator : MonoBehaviour
{
    private const double TOP_LAT = -37.63897208;
    private const double LEFT_LONG = 174.92053902;
    private const int TERRAIN_RESOLUTION = 8;
    private const int SATELLITE_RESOLUTION = 10;
    public static float UPPER_HEIGHT = 3000.0f; // arbitrary height selected, placeholder for now
    public Material satelliteMaterial;

    private const int TERRAIN_WIDTH = 24576;
    private const int TERRAIN_HEIGHT = 32767;

    private byte[] elevations;
    private byte[] satellite;

    private int satelliteWidth, satelliteHeight;
    
    private volatile Slider progressBar;
    public volatile Text progressMessage;

    void Start()
    {
        StartCoroutine(loadFunction());
    }

    IEnumerator loadFunction()  // very unclean, but it works
    {
        yield return null;
        progressMessage.text = "Loading Terrain Data";
        yield return null;
        loadTerrainData();
        progressMessage.text = "Loading Satellite Data";
        yield return null;
        loadSatelliteData();
        progressMessage.text = "Adding Elevation to Map";
        yield return null;
        double latitude = double.Parse(PlayerPrefs.GetString("latitude"));
        double longitude = double.Parse(PlayerPrefs.GetString("longitude"));
        int width = PlayerPrefs.GetInt("width");
        addElevationOntoTerrain(latitude, longitude, width);
        progressMessage.text = "Adding Satellite Texture";
        yield return null;
        imposeSatelliteData(latitude, longitude, width);
        progressMessage.text = "Creating additional overlays";
        yield return null;
        this.GetComponent<AdditionalLayerMaker>().addElevationOverlay();
        yield return null;
        progressMessage.text = "Creating geology map";
        GetComponent<AdditionalLayerMaker>().addGeologyMapOverlay();
        yield return null;
        progressMessage.text = "Loading Earthquake Data";
        yield return null;
        this.GetComponent<DataHandler>().loadData();
        progressMessage.text = "Done!";
        Destroy(GameObject.Find("LoadingScreen"));
        yield return null;
    }

    private void loadTerrainData()
    {
        TextAsset terrainFile = Resources.Load("terrain") as TextAsset;
        elevations = terrainFile.bytes;
    }

    private void loadSatelliteData()
    {
        TextAsset satelliteFile = Resources.Load("satellite") as TextAsset;
        satellite = satelliteFile.bytes;
        satelliteWidth = (satellite[0] << 8) | satellite[1];
        satelliteHeight = (satellite[2] << 8) | satellite[3];
    }

    public void addElevationOntoTerrain(double latitude, double longitude, int width)
    {
        double[] nztm = CoordinateConverter.geographic_to_transverse_mercator(latitude, longitude);
        double[] originNZTM = CoordinateConverter.geographic_to_transverse_mercator(TOP_LAT, LEFT_LONG);
        double metersFromTop = Math.Abs(nztm[1] - originNZTM[1]); //Math.Abs(latitude - TOP_LAT) * 110.574 * 1000;
        double metersFromLeft = Math.Abs(nztm[0] - originNZTM[0]); // (longitude - LEFT_LONG) * 111.321 * Math.Abs(Math.Cos(Math.PI * latitude / 180)) * 1000;
        if (metersFromTop < (width / 2) * 1000 || metersFromLeft < (width / 2) * 1000) // check to see if extent is not out of bounds
        {
            Debug.LogWarning("Selected location and width out of bounds");
            return;
        }
        Terrain terrain = Terrain.activeTerrain;
        float[,] heights = terrain.terrainData.GetHeights(0, 0, terrain.terrainData.heightmapTexture.width, terrain.terrainData.heightmapTexture.height);
        double widthInCells = (width * 1000) / TERRAIN_RESOLUTION;
        double terrainToResolutionRatio = widthInCells / terrain.terrainData.heightmapTexture.width;
        int startY = (int)(metersFromTop / TERRAIN_RESOLUTION) - (int)(widthInCells / 2);
        int startX = (int)(metersFromLeft / TERRAIN_RESOLUTION) - (int)(widthInCells / 2);
        for (int i = 0; i < terrain.terrainData.heightmapTexture.height; i++)
        {
            for (int j = 0; j < terrain.terrainData.heightmapTexture.width; j++)
            {
                ushort cell = getElevationAt(startY + (int)(i * terrainToResolutionRatio), startX + (int)(j * terrainToResolutionRatio));
                float elevation = (float)cell / UPPER_HEIGHT;
                heights[j, i] = elevation;
            }
        }
        terrain.terrainData.SetHeights(0, 0, heights);
        elevations = null;
    }

    private ushort getElevationAt(int y, int x)
    {
        int index = (y * TERRAIN_WIDTH * 2) + (y - 1) + (x * 2) - 1;
        return (ushort)((elevations[index] << 8) | elevations[index + 1]);
    }

    private void imposeSatelliteData(double latitude, double longitude, int width)
    {
        double[] nztm = CoordinateConverter.geographic_to_transverse_mercator(latitude, longitude); // should be 1852436.307 5702378.263
        double[] topNZTM = CoordinateConverter.geographic_to_transverse_mercator(TOP_LAT, LEFT_LONG);
        Terrain terrain = this.GetComponent<Terrain>();
        double metersFromTop = Math.Abs(nztm[1] - topNZTM[1]);
        double metersFromLeft = Math.Abs(nztm[0] - topNZTM[0]);
        double widthInCells = (width * 1000) / SATELLITE_RESOLUTION;
        int startX = (int)(metersFromLeft / SATELLITE_RESOLUTION) - (int)(widthInCells / 2);
        int startY = (int)(metersFromTop / SATELLITE_RESOLUTION) - (int)(widthInCells / 2);
        int count = (int)widthInCells;
        Texture2D output = new Texture2D(count, count);
        for (int i = 0; i < count; i++)
        {
            for (int j = 0; j < count; j++)
            {
                Color color = getSatelliteAt(startY + i, startX + j);
                output.SetPixel(j, count - 1 - i, color); 
            }
        }
        output.Apply();
        output = rotateTexture(output, false);
        var data = terrain.terrainData;
        TerrainLayer[] layers = new TerrainLayer[1];
        layers[0] = new TerrainLayer();
        layers[0].diffuseTexture = output;
        layers[0].tileOffset = new Vector2(0, 0);
        layers[0].tileSize = new Vector2(1000, 1000);
        layers[0].smoothness = 0.0f;
        terrain.terrainData.terrainLayers = layers;
        float[,,] alpha = new float[data.alphamapResolution, data.alphamapResolution, 1];
        for (int i = 0; i < alpha.GetLength(0); i++)
        {
            for (int j = 0; j < alpha.GetLength(1); j++)
            {
                for (int k = 0; k < alpha.GetLength(2); k++)
                {
                    alpha[i, j, k] = 1;
                }
            }
        }
        terrain.terrainData.SetAlphamaps(0, 0, alpha);
        satellite = null;
        // testTexture();
    }

    private Color getSatelliteAt(int y, int x)
    {
        int index = (y * satelliteWidth * 3) + 4 + (x * 3);
        byte r = satellite[index];
        byte g = satellite[index + 1];
        byte b = satellite[index + 2];
        return new Color(r / 255.0f, g / 255.0f, b / 255.0f);
    }

    public static Texture2D rotateTexture(Texture2D originalTexture, bool clockwise)
    {
        Color[] original = originalTexture.GetPixels();
        Color[] rotated = new Color[original.Length];
        int w = originalTexture.width;
        int h = originalTexture.height;
        int indexRotated, indexOriginal;
        for (int j = 0; j < h; ++j)
        {
            for (int i = 0; i < w; ++i)
            {
                indexRotated = (i + 1) * h - j - 1;
                indexOriginal = clockwise ? original.Length - 1 - (j * w + i) : j * w + i;
                rotated[indexRotated] = original[indexOriginal];
            }
        }
        Texture2D rotatedTexture = new Texture2D(w, h);
        rotatedTexture.SetPixels(rotated);
        rotatedTexture.Apply();
        return rotatedTexture;
    }
}
