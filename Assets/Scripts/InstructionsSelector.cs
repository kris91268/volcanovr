using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstructionsSelector : MonoBehaviour
{
    public Image image;

    public Sprite oculusImage;
    public Sprite viveImage;

    // Start is called before the first frame update
    void Start()
    {
        string platform = PlayerPrefs.GetString("platform");
        if (platform.Equals("oculus"))
        {
            image.sprite = oculusImage;
            Debug.Log("Set instructions to oculus");
        }
        else if (platform.Equals("vive"))
        {
            image.sprite = viveImage;
            Debug.Log("Set instructions to vive");
        }
        else
        {
            Debug.Log("No instruction set found for this system");
        }
    }
}
