using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GraphDisplayMenuHandler : MonoBehaviour
{
    public Image graph;
    public Canvas canvas;

    public void setGraphImage(TimeGraph dataGraph)
    {
        var g = dataGraph.getGraph();
        graph.sprite = Sprite.Create(g, new Rect(0f, 0f, g.width, g.height), new Vector2(g.width / 2.0f, g.height / 2.0f));
    }

    public void registerCamera(Camera camera)
    {
        canvas.worldCamera = camera;
    }

    public void onCloseButtonPressed()
    {
        Destroy(gameObject);
    }
}
