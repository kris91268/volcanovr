using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;

public class PresetLoader : MonoBehaviour
{
    public GameObject panel;
    public GameObject buttonPrefab;

    private const string DIRECTORY = "Presets\\";

    // Start is called before the first frame update
    void Start()
    {
        if (!Directory.Exists(DIRECTORY))
        {
            Directory.CreateDirectory(DIRECTORY);
        }
        string[] entries = Directory.GetFiles(DIRECTORY);
        Debug.Log($"Found {entries.Length} potential presets to load");
        foreach (string entry in entries)
        {
            GameObject button = Instantiate(buttonPrefab, panel.GetComponent<VerticalLayoutGroup>().transform);
            try
            {
                string[] lines = File.ReadAllLines(entry);
                string name = lines[1].Split('=')[1];
                button.transform.GetChild(0).gameObject.GetComponent<Text>().text = name.Substring(1, name.Length - 2);
            } 
            catch (IOException exc)
            {
                Debug.Log($"Failed to load file {entry}. See below error message.");
                Debug.Log(exc.Message);
            }
            button.GetComponent<Button>().onClick.AddListener(delegate { loadPreset(entry); });
        }
    }

    private void loadPreset(string path)
    {
        try
        {
            string[] lines = File.ReadAllLines(path);
            PlayerPrefs.SetString("latitude", lines[2].Split('=')[1]);
            PlayerPrefs.SetString("longitude", lines[3].Split('=')[1]);
            PlayerPrefs.SetInt("width", int.Parse(lines[4].Split('=')[1]));
            PlayerPrefs.SetString("preset", path);
            SceneManager.LoadScene(1);
        }
        catch (IOException exc)
        {
            Debug.Log($"Failed to load file {path}");
            Debug.Log(exc.Message);
        }
    }
}
