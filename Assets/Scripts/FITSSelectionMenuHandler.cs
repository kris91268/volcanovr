using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using DotnetDataLens;

public class FITSSelectionMenuHandler : MonoBehaviour
{
    public Dropdown dataSelection;
    public InputField startDateField, endDateField;
    public Canvas mainCanvas;
    public Text siteIDText;

    public GameObject graphWindowPrefab;

    public void registerCamera(Camera camera)
    {
        mainCanvas.worldCamera = camera;
    }

    public void onDataButtonPress()
    {
        #pragma warning disable 168 // I don't need warnings about unused variables here
        try
        {
            Datalens lens = new Datalens();
            var options = dataSelection.options;
            if (startDateField.text.Equals("") && endDateField.text.Equals(""))
            {
                TimeGraph graph = new TimeGraph(lens.fits.getDataOfTypeFromSite(siteIDText.text, options[dataSelection.value].text));
                var obj = Instantiate(graphWindowPrefab);
                obj.transform.position = transform.position;
                obj.transform.rotation = transform.rotation;
                obj.transform.localScale = transform.localScale;
                var component = obj.GetComponent<GraphDisplayMenuHandler>();
                component.registerCamera(mainCanvas.worldCamera);
                component.setGraphImage(graph);
                Destroy(gameObject);
                return;
            }
            DateTime start = isValidDate(startDateField.text);
            DateTime end = isValidDate(endDateField.text);
            TimeGraph rangedGraph = new TimeGraph(lens.fits.getDataOfTypeFromSiteWithinRange(siteIDText.text, options[dataSelection.value].text, start, end));
            var g = Instantiate(graphWindowPrefab);
            g.transform.position = transform.position;
            g.transform.rotation = transform.rotation;
            g.transform.localScale = transform.localScale;
            var comp = g.GetComponent<GraphDisplayMenuHandler>();
            comp.registerCamera(mainCanvas.worldCamera);
            comp.setGraphImage(rangedGraph);
            Destroy(gameObject);

        }
        catch (Exception exc)
        {
            Debug.Log("invalid date range for FITS data selection");
            Debug.Log(exc.Message);
            return;
        }
        #pragma warning restore 168
    }

    public void onCancelButtonPress()
    {
        Destroy(this.gameObject);
    }

    public void setSiteID(string id)
    {
        siteIDText.text = id;
    }

    public void setOptions(string[] typeIDNames)
    {
        List<Dropdown.OptionData> options = new List<Dropdown.OptionData>();
        foreach (string s in typeIDNames)
        {
            options.Add(new Dropdown.OptionData(s));
        }
        dataSelection.AddOptions(options);
    }

    private static DateTime isValidDate(string text)
    {
        #pragma warning disable 168
        try
        {
            string[] parts = text.Split('/');
            int day = int.Parse(parts[0]);
            int month = int.Parse(parts[1]);
            int year = int.Parse(parts[2]);
            if (day < 1 || day > 31 || month < 1 || month > 12 || year < 2000)
            {
                throw new ArgumentException("invalid date");
            }
            return new DateTime(year, month, day);
        }
        catch (Exception exc)
        {
            throw new ArgumentException("Invalid date");
        }
        #pragma warning restore 168
    }
}
