using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteDrawer : MonoBehaviour
{
    public Sprite sprite;

    private void Awake()
    {
        var renderer = GetComponent<MeshRenderer>();
        var mesh = GetComponent<MeshFilter>().mesh;
        var material = renderer.material;
        Rect spriteRect = sprite.textureRect;

        spriteRect.x /= sprite.texture.width;
        spriteRect.width /= sprite.texture.width;
        spriteRect.y /= sprite.texture.height;
        spriteRect.height /= sprite.texture.height;

        Vector2[] uvs = mesh.uv;
        uvs[0] = new Vector2(0.0f, 1.0f);
        uvs[1] = new Vector2(1.0f, 1.0f);
        uvs[2] = new Vector2(0.0f, 0.0f);
        uvs[3] = new Vector2(1.0f, 0.0f);
        mesh.uv = uvs;
        material.mainTexture = sprite.texture;
    }
}
