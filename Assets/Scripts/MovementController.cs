using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using UnityEngine.SceneManagement;
using DotnetDataLens;

public class MovementController : MonoBehaviour
{
    public float moveSpeed;
    public float mouseSensitivity;
    public GameObject radialOverlaySelectorPrefab;
    public GameObject earthquakeDetailsPrefab;
    public GameObject fitsMenuPrefab;
    public GameObject geologyMenuPrefab;

    public SteamVR_Input_Sources leftHand, rightHand;
    public SteamVR_Action_Boolean trigger;
    public SteamVR_Action_Boolean grip;
    public SteamVR_Action_Vector2 joystick;
    public SteamVR_Action_Boolean secondaryButton;
    public SteamVR_Action_Boolean primaryButton;
    public GameObject mainCamera;
    public GameObject leftController, rightController;

    private Transform mainCameraTransform;

    private bool overlayMenuOpen = false;
    private GameObject openedMenu;
    private float timeWhenOpened;

    private bool laserPointerActive = false;
    public GameObject laserPointer;

    [HideInInspector]
    public bool earthquakeDetailsPrefabActive;

    private static int MENU_SCENE_VALUE = 0;

    private const float MENU_OFFSET = 18.0f;
    private const float MENU_SCALE = 0.03f;

    [HideInInspector]
    public bool geologyDetailsOpen = false;

    public UserPosition positionData;
    public GameObject locationDisplayPrefab;
    private GameObject locationDisplay;
    private bool locationOpened = false;

    // Start is called before the first frame update
    void Start()
    {
        laserPointer.SetActive(false);
        mainCameraTransform = mainCamera.transform;
    }

    // Update is called once per frame
    void Update()
    {
        processVRMovement();
    }

    private void processVRMovement()
    {
        if (trigger.GetState(leftHand) && !overlayMenuOpen) // move foward case
        {
            float localMoveSpeed = moveSpeed;
            Vector2 axes = joystick.GetAxis(leftHand);
            localMoveSpeed *= (1.0f + (axes.y * 2.0f));
            transform.position += (mainCameraTransform.forward * localMoveSpeed);
        }
        if (trigger.GetState(rightHand) && !overlayMenuOpen && !laserPointerActive) // move back case
        {
            transform.position += (-mainCameraTransform.forward * moveSpeed);
        }
        if (grip.GetStateDown(leftHand) && !overlayMenuOpen) // open the overlay menu
        {
            overlayMenuOpen = true;
            Debug.Log("Opening overlay selector");
            openedMenu = Instantiate(radialOverlaySelectorPrefab, leftController.transform);
            timeWhenOpened = Time.time;
        }
        if (grip.GetStateUp(leftHand) && overlayMenuOpen) // close the overlay menu
        {
            overlayMenuOpen = false;
            Debug.Log("Closing overlay selector");
            Destroy(openedMenu);
        }
        if (trigger.GetStateDown(leftHand) && overlayMenuOpen) // case for selecting a new overlay
        {
            overlayMenuOpen = false;
            Debug.Log("Closing overlay selector and setting overlay");
            GameObject.Find("Terrain").GetComponent<AdditionalLayerMaker>().switchOverlay(openedMenu.GetComponent<OverlaySelector>().currentOverlay);
            Destroy(openedMenu);
        }
        if (grip.GetStateDown(rightHand)) // toggle laser pointer
        {
            laserPointer.SetActive(!laserPointerActive);
            laserPointerActive = !laserPointerActive;
        }
        if (trigger.GetStateDown(rightHand) && laserPointerActive) // handle interactions
        {
            RaycastHit raycast = GameObject.Find("Pointer").GetComponent<VRVisualizationLaserPointer>().getRaycastHit();
            if (raycast.collider.gameObject.name == "Earthquake")
            {
                var quakes = GameObject.Find("Terrain").GetComponent<DataHandler>().currentQuakes;
                foreach (var quake in quakes)
                {
                    if (quake.gameObject.Equals(raycast.collider.gameObject))
                    {
                        Vector3 pos = mainCameraTransform.position + (mainCameraTransform.forward * MENU_OFFSET);
                        GameObject prefab = Instantiate(earthquakeDetailsPrefab);
                        prefab.transform.position = pos;
                        prefab.transform.localScale = new Vector3(MENU_SCALE, MENU_SCALE, MENU_SCALE);
                        prefab.transform.LookAt(mainCameraTransform);
                        prefab.GetComponent<EarthquakeDetailsPrefabController>().setAll(quake.latitude, quake.longitude, quake.time, quake.depth, quake.magnitude);
                        prefab.GetComponent<EarthquakeDetailsPrefabController>().registerCamera(laserPointer.GetComponent<Camera>());
                    }
                }
            }
            if (raycast.collider.gameObject.name.StartsWith("FITS:"))
            {
                string siteID = raycast.collider.gameObject.name.Split(':')[1].Trim();
                Vector3 pos = mainCameraTransform.position + (mainCameraTransform.forward * MENU_OFFSET);
                GameObject menu = Instantiate(fitsMenuPrefab);
                menu.transform.position = pos;
                menu.transform.localScale = new Vector3(MENU_SCALE, MENU_SCALE, MENU_SCALE);
                menu.transform.LookAt(mainCameraTransform);
                menu.transform.Rotate(new Vector3(0.0f, 180.0f, 0.0f));
                var handler = menu.GetComponent<FITSSelectionMenuHandler>();
                handler.registerCamera(laserPointer.GetComponent<Camera>());
                handler.setSiteID(siteID);
                handler.setOptions(FitsStationMethods.getMethodsFromSite(siteID));
            }
            if (raycast.collider.gameObject.name.Equals("Terrain") && 
                GameObject.Find("Terrain").GetComponent<AdditionalLayerMaker>().currentOverlay == AdditionalLayerMaker.Overlays.GEOLOGY_MAP)
            {
                handleGeologyClick(raycast);
            }
        }
        if (primaryButton.GetStateDown(leftHand)) // go back to main menu
        {
            SceneManager.LoadScene(MENU_SCENE_VALUE);
        }
        if (secondaryButton.GetStateDown(leftHand)) // change earthquake data type
        {
            var obj = GameObject.Find("Terrain").GetComponent<EarthquakeDataChanger>();
            var currentType = obj.getCurrentType();
            if (currentType == EarthquakeDataChanger.EarthquakeDataType.TIME)
            {
                obj.changeEarthquakeDataDisplay(EarthquakeDataChanger.EarthquakeDataType.MAGNITUDE);
            } 
            else
            {
                obj.changeEarthquakeDataDisplay(EarthquakeDataChanger.EarthquakeDataType.TIME);
            }
        }
        if (secondaryButton.GetStateDown(rightHand)) // show/hide location
        {
            if (!locationOpened)
            {
                Debug.Log("Showing display");
                locationDisplay = Instantiate(locationDisplayPrefab, rightController.transform);
                locationDisplay.GetComponent<LocationDisplayUpdater>().registerUserPosition(positionData);
                // locationDisplay.transform.position = new Vector3(0.0f, 3.0f, -10.0f);
                locationDisplay.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            }
            else
            {
                Destroy(locationDisplay);
            }
            locationOpened = !locationOpened;
        }
    }

    private void handleGeologyClick(RaycastHit hit)
    {
        if (geologyDetailsOpen)
        {
            return;
        }
        AdditionalLayerMaker layers = GameObject.Find("Terrain").GetComponent<AdditionalLayerMaker>();
        int x = (int)((hit.point.z / 1000.0f) * 2000.0f);
        int y = (int)((hit.point.x / 1000.0f) * 2000.0f);
        GeologyMapRequest geoDetails = layers.map.getGeologyFeature(x, y);
        FaultMapRequest faultDetails = layers.map.getFaultFeature(x, y);
        GameObject geologyDetails = Instantiate(geologyMenuPrefab);
        geologyDetails.transform.position = transform.position + (transform.forward * MENU_OFFSET);
        geologyDetails.transform.localScale = new Vector3(MENU_SCALE, MENU_SCALE, MENU_SCALE);
        geologyDetails.transform.LookAt(transform);
        geologyDetails.transform.Rotate(0.0f, 180.0f, 0.0f);
        var component = geologyDetails.GetComponent<GeologyDetailMenuHandler>();
        component.registerCamera(laserPointer.GetComponent<Camera>());
        component.registerMovementController(this);
        component.addFeatureRequests(geoDetails, faultDetails);
        geologyDetailsOpen = true;
    }

    /*private void handleRotation()
    {
        camRot *= Quaternion.Euler(-Input.GetAxis("Mouse Y") * mouseSensitivity, 0f, 0f);
        parentCamRot *= Quaternion.Euler(0f, Input.GetAxis("Mouse X") * mouseSensitivity, 0f);
        mainCameraTransform.localRotation = camRot;
        cameraParentTransform.localRotation = parentCamRot;
    }*/
}
