using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DotnetDataLens;
using System;

public class GeologyDetailMenuHandler : MonoBehaviour
{
    public Text latitudeText, longitudeText;
    public Text labelsText, contentText;
    public Canvas mainCanvas;

    private const int MAX_LINE_LENGTH = 40;

    private MovementController mc;

    public void registerCamera(Camera camera)
    {
        mainCanvas.worldCamera = camera;
    }

    public void enterCoordinates(double lat, double ln)
    {
        latitudeText.text = $"{lat} S";
        longitudeText.text = $"{ln} E";
    }

    public void onCloseButtonPressed()
    {
        mc.geologyDetailsOpen = false;
        Destroy(gameObject);
    }

    public void registerMovementController(MovementController mc)
    {
        this.mc = mc;
    }

    public void addFeatureRequests(GeologyMapRequest geology, FaultMapRequest faults)
    {
        string labels = "Geology Units:\nGeology Units Total:\nCode:\nUnit Code:\nMain Rock:\nSub Rock:\nMap Unit:\n";
        labels += "Stratigrahic Unit:\nStratlex:\nSequence:\nTerrane:\nSupergroup:\nGroup:\nSubgroup:\nFormation:\n";
        labels += "Member:\nProtolith:\nT-zone:\nStratigrahpic Age:\nAge Min:\nAge Max:\nConfidence:\nDescription:\n";
        labels += "Rock Group:\nRock Class:\nUnique Code:\nText Code:\nSimple Name:\nKey Name:\nKey Group:\nQMap Name:\nQMap Number:";
        if (!faults.name.Equals("no fault found"))
        {
            labels += "\n-----------------------\nLength:\nIdentifier:\nAccuracy:\nDominant Sense:\nSub Sense:\nActivity:\nPlot Rank:\n";
            labels += "Name:\nZone:\nType:\nRDom Sense:\nRSub Sense:\nRType:\nDip Direction:\nDip:\nAge:\nTotal Slip:\n";
            labels += "Down Quadrant:\nQMap Name:\nQMap Number:";
        }
        string content = $"{geology.geologyUnits}\n{geology.geologyUnitsTotal}\n{geology.code}\n{geology.unitCode}\n{geology.mainRock}\n";
        content += $"{geology.subRocks}\n{geology.mapUnit}\n{geology.stratigraphicUnit}\n{geology.stratlex}\n{geology.sequence}\n{geology.terrane}\n";
        content += $"{geology.supergroup}\n{geology.group}\n{geology.subgroup}\n{geology.formation}\n{geology.member}\n{geology.protolith}\n{geology.tzone}\n";
        content += $"{geology.stratAge}\n{geology.absoluteAgeMin}\n{geology.absoluteAgeMax}\n{geology.confidence}\n{geology.description}\n";
        content += $"{geology.rockGroup}\n{geology.rockClass}\n{geology.uniqueCode}\n{geology.textCode}\n{geology.simpleName}\n{geology.keyName}\n";
        content += $"{geology.keyGroup}\n{geology.qmapName}\n{geology.qmapNumber}";
        if (!faults.name.Equals("no fault found"))
        {
            content += $"\n-----------------------\n{faults.length}\n{faults.identifier}\n{faults.accuracy}\n{faults.dominantSense}\n{faults.subSense}\n";
            content += $"{faults.activity}\n{faults.plotRank}\n{faults.name}\n{faults.zone}\n{faults.type}\n{faults.rdomSense}\n{faults.rsubSense}\n{faults.rtype}\n";
            content += $"{faults.dipDir}\n{faults.dip}\n{faults.age}\n{faults.totalSlip}\n{faults.downQuad}\n{faults.qmapName}\n{faults.qmapNumber}";
        }
        labels = pad(labels, content);
        labelsText.text = labels;
        contentText.text = content;
    }

    private static string pad(string labels, string content) // inserts the appropriate amount of \n's to evenly align labels and content
    {
        string[] labelList = labels.Split('\n');
        string[] contentList = content.Split('\n');
        if (labelList.Length != contentList.Length)
        {
            throw new ArgumentException($"Label and content item count do not match (this should never happen): {labelList.Length} vs {contentList.Length}");
        }
        for (int i = 0; i < labelList.Length; i++)
        {
            int paddingAmount = (contentList[i].Length - contentList[i].Length % MAX_LINE_LENGTH) / MAX_LINE_LENGTH;
            for (int j = 0; j < paddingAmount; j++)
            {
                labelList[i] += '\n';
            }
        }
        string final = "";
        foreach (string s in labelList)
        {
            final += s + '\n';
        }
        return final;
    }
}
