Shader "Custom/TransparentTerrainShader" {
	Properties{
		// [HideInInspector] _Control("Control (RGBA)", 2D) = "red" {}
		[HideInInspector] _MainTex("BaseMap (RBG) Trans (A)", 2D) = "white" {}
		[HideInInspector] _Color("Main Color", Color) = (1,1,1,1)
		_Transparency("Transparency", Range(0.0, 1.0)) = 0.5
	}

	SubShader{
		Tags {"Queue"="Transparent" "RenderType"="Transparent"}
		CGPROGRAM
			#pragma surface surf Lambert alpha 
			#pragma target 3.0

			struct Input {
				float2 uv_MainTex;
			};

			sampler2D _MainTex;
			float _Transparency;

			void surf(Input IN, inout SurfaceOutput o) {
				o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
				o.Alpha = _Transparency;
			}
		ENDCG
	}
	Fallback "Diffuse"
}