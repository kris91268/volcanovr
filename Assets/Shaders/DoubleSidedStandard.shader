Shader "Custom/DoubleSidedStandard"
{
    Properties
    {
        _Color ("Color", Color) = (0,0,0,1)
        _MainTex ("Albedo (RGB)", 2D) = "black" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.293
        _Metallic ("Metallic", Range(0,1)) = 0.495
        _BumpMap("NormalMap", 2D) = "Bump" {}
        _BumpScale("Normal Power", Float) = 1.0
        _NormalDefault("Normal Default", Color) = (128, 128, 255)
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 200
        Cull Front
        CGPROGRAM

        #pragma surface surf Standard fullforwardshadows
        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _BumpMap;
        
        struct Input {
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _BumpScale;
        half _Metallic;
        fixed4 _Color;
        half3 _NormalDefault;

        void surf(Input IN, inout SurfaceOutputStandard o) {
            fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Normal = -UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
            o.Alpha = c.a;
        }
        ENDCG

        Cull Back
        CGPROGRAM

        #pragma surface surf Standard fullforwardshadows
        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _BumpMap;

        struct Input {
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _BumpScale;
        half _Metallic;
        fixed4 _Color;
        half3 _NormalDefault;

        void surf(Input IN, inout SurfaceOutputStandard o) {
            fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
