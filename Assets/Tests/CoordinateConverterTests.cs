using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System;

public class CoordinateConverterTests
{
    /*
     * Actual values obtained from the LINZ coordinate converter web service 
     * https://www.geodesy.linz.govt.nz/concord/index.cgi
     * 
     * Uses NZGD2000 and NZTM
     */

    private const double deltaGT = 0.01;  // to how many decimal places should the value be correct for?
    private const double deltaTG = 0.00000001;

    [Test]
    public void geographicTransverseTest1()
    {
        double[] result = CoordinateConverter.geographic_to_transverse_mercator(-39.01532, 176.12911);
        Assert.AreEqual(1870924.892, result[0], deltaGT);
        Assert.AreEqual(5676863.482, result[1], deltaGT);
    }

    [Test]
    public void geographicTransverseTest2()
    {
        double[] result = CoordinateConverter.geographic_to_transverse_mercator(-39.0, 176.0);
        Assert.AreEqual(1859800.192, result[0], deltaGT);
        Assert.AreEqual(5678940.884, result[1], deltaGT);
    }

    [Test]
    public void geographicTransverseTest3()
    {
        double[] result = CoordinateConverter.geographic_to_transverse_mercator(-38.25, 175.5);
        Assert.AreEqual(1818762.458, result[0], deltaGT);
        Assert.AreEqual(5763490.590, result[1], deltaGT);
    }

    [Test]
    public void invalidLatitudeGeographicTransverseTest1()
    {
        Assert.Throws<ArgumentException>(delegate { CoordinateConverter.geographic_to_transverse_mercator(12.0, 175.5); });
    }

    [Test]
    public void invalidLatitudeGeographicTransverseTest2()
    {
        Assert.Throws<ArgumentException>(delegate { CoordinateConverter.geographic_to_transverse_mercator(-93.0, 175.5); });
    }

    [Test]
    public void invalidLongitudeGeographicTransverseTest1()
    {
        Assert.Throws<ArgumentException>(delegate { CoordinateConverter.geographic_to_transverse_mercator(-38.0, 17.0); });
    }

    [Test]
    public void invalidLongitudeGeographicTransverseTest2()
    {
        Assert.Throws<ArgumentException>(delegate { CoordinateConverter.geographic_to_transverse_mercator(-38.0, 420.69); });
    }

    [Test]
    public void transverseGeographicTest1()
    {
        double[] result = CoordinateConverter.transverse_mercator_to_geographic(1628677, 5313457);
        Assert.AreEqual(173.34807422, result[0], deltaTG);
        Assert.AreEqual(-42.33060825, result[1], deltaTG);
    }

    [Test]
    public void transverseGeographicTest2()
    {
        double[] result = CoordinateConverter.transverse_mercator_to_geographic(1700000, 5500000);
        Assert.AreEqual(174.18269962, result[0], deltaTG);
        Assert.AreEqual(-40.64479965, result[1], deltaTG);
    }

    [Test]
    public void transverseGeographicTest3()
    {
        double[] result = CoordinateConverter.transverse_mercator_to_geographic(1800000, 5700000);
        Assert.AreEqual(175.30392504, result[0], deltaTG);
        Assert.AreEqual(-38.82609254, result[1], deltaTG);
    }

    [Test]
    public void invalidEastingTransverseGeographicTest1()
    {
        Assert.Throws<ArgumentException>(delegate { CoordinateConverter.transverse_mercator_to_geographic(1300000, 5902000); });
    }

    [Test]
    public void invalidEastingTransverseGeographicTest2()
    {
        Assert.Throws<ArgumentException>(delegate { CoordinateConverter.transverse_mercator_to_geographic(3000000, 5902000); });
    }

    [Test]
    public void invalidNorthingTransverseGeographicTest1()
    {
        Assert.Throws<ArgumentException>(delegate { CoordinateConverter.transverse_mercator_to_geographic(1700000, 3000000); });
    }

    [Test]
    public void invalidNorthingTransverseGeographicTest2()
    {
        Assert.Throws<ArgumentException>(delegate { CoordinateConverter.transverse_mercator_to_geographic(1700000, 7000000); });
    }
}
