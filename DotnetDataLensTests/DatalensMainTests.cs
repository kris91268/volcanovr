﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using DotnetDataLens;

namespace DotnetDataLensTests
{
    [TestClass]
    public class DatalensMainTests
    {
        [TestMethod]
        public void datalensTest1()
        {
            Datalens lens = new Datalens();
            List<Fdsn.FdsnEvent> events = lens.fdsn.getEventsWithinTimeAndBounds("2021-01-01T12:00:00", "2021-06-29T12:00:00", -38.2, 176.3, -37.9, 176.6);
            Assert.IsTrue(events.Count > 0);
        }

        [TestMethod]
        public void datalensTest2()
        {
            Datalens lens = new Datalens();
            List<Fits.FitsSite> sites = lens.fits.getSitesInBounds(-38.2, 176.3, -37.9, 176.6);
            Assert.IsTrue(sites.Count > 0);
        }

        [TestMethod]
        public void datalensTestNamePresent()
        {
            Datalens lens = new Datalens();
            FitsData data = lens.fits.getDataOfTypeFromSite("WI032", "t");
            Assert.AreEqual(data.siteID, "WI032");
            Assert.AreEqual(data.typeID, "t");
        }

        [TestMethod]
        public void testIfNonZeroDataFromSiteIsPresent1()
        {
            Datalens lens = new Datalens();
            FitsData data = lens.fits.getDataOfTypeFromSite("WI032", "t");
            bool flag = false;
            foreach (var entry in data.data[0].data)
            {
                if (entry != 0.0)
                {
                    flag = true;
                }
            }
            Assert.IsTrue(flag);
        }
    }
}
