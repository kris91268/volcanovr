## What is VolcanoVR?
VolcanoVR is a *proof of concept* visualisation tool for viewing volcanic data within Virtual Reality (VR). Using a section of the [Taupo Volcanic Zone](https://en.wikipedia.org/wiki/Taup%C5%8D_Volcanic_Zone) (TVZ) in New Zealand and a link to the [GeoNet API](https://api.geonet.org.nz/), a 3D representation of the landscape with relevant volcanic data (earthquakes, GNSS, gas/water chemistry etc.) is constructed and viewable in an immersive VR environment (with added support for desktop usage). VolcanoVR is supported on both the HTC Vive and Oculus Quest 2 through SteamVR. A wired or AirLink connection with a computer running VolcanoVR through SteamVR is required for use on the Oculus Quest 2. This project (and the user survey results) were part of a Masters of Science degree at Victoria University of Wellington. The thesis paper can be found [here](https://doi.org/10.26686/wgtn.21077260).

## What is VolcanoVR designed for?
VolcanoVR is designed as a multipurpose tool for the three main purposes of monitoring, research, and outreach/education with an example focus of the **central TVZ** of New Zealand. Using this tool, you can view earthquake data for a predefined volcanic area (or provide NZTM-2000 coordinates for your own region within the central TVZ) during a user-defined period of time. 

## How do I apply VolcanoVR to my own area of interest?
Unfortunately, VolcanoVR is currently **hard set** to the TVZ and GeoNet data. Significant backend work will need to be done on the software to show a different location with different providers of volcanic data livestreamed in. Contact the lead author and developer ([Kristian Hansen](https://orcid.org/my-orcid?orcid=0009-0004-2129-2484)) if you would like to apply your own region and datasets to the VolcanoVR environment and I will assist you. Further, in the future a flexible and robust VolcanoVR 2.0 is planned, which will allow users to define any volcanic area and configure livestream services of volcanic data into VolcanoVR. 

However, you can load your own earthquake data into VolcanoVR, provided it resides within the TVZ. See the **How do I view my own TVZ earthquake data?** section to learn more.

## Installation and Setup
VolcanoVR is supported on both the HTC Vive and Oculus Quest 2 through SteamVR, or on a Windows desktop environment. Download the main branch on the GitLab repository if you want the VR version, or download the 'pc_version' branch for the Windows Desktop version. VolcanoVR was constructed in the Unity Engine (version **2021.1.4f1**) and so this is required to build the project yourself (more on installing Unity and setting it up [here](https://unity.com/download)). Note that a [Steam](https://store.steampowered.com/) account is required for SteamVR and VR headset integration to work. From there, you can either run VolcanoVR in the Unity Editor, or build the project through Unity to an executable and use that.

## Controls
Note movement forwards/backwards will propel you in the **direction you are facing**.
### Quest 2
- `Left Joystick`: Select overlay/move faster
- `Y`: Return to Main menu
- `X`: Toggle earthquake display (magnitude/time)
- `A`: Toggle location display
- `Left Grip`: Hold to open overlay selector (then navigate with `Left Joystick`)
- `Right Grip`: Toggle Laser Pointer
- `Right Trigger`: Interact/examine, Move back when laser pointer is not active
- `Left Trigger`: Click overlay/Move in direction you are facing
### HTC Vive
- `Left Touchpad Down`: Toggle earthquake display (magnitude/time)
- `Right Touchpad Down`: Show current location display
- `Left Touchpad Up`: Return to Main menu
- `Left Grip`: Hold to open overlay selector (then navigate with `Left Touchpad`)
- `Right Grip`: Toggle laser pointer
- `Left Trigger`: Choose overlay (while overlay selector is open), Move forward in facing direction
- `Right Trigger`: Move back/interact (while laser pointer active)
### Keyboard/Mouse
- `W/S/A/D`: Move Forward/Back/Left/Right
- `Left Shift`: Hold to move faster
- `Tab`: Hold to disable camera mouse look (use cursor)
- `Left Ctrl`: Cycle earthquake data type (changes colour of earthquakes between Magnitude and Time)
- `Left Mouse Click`: Query object/location at mouse position. Interact with earthquakes or FITS stations, or query geology data when geological overlay is active.
- `Escape`: Go back to the main menu
- `Space`: Cycle through terrain overlays (satellite, topograhy, geological data overlay)

## How to use VolcanoVR
When launching VolcanoVR, you will be greeted with a main menu with many options. The central window will let you select where you want to view and the timespan of earthquake data to view for this region. Note due to constraints with the GeoNet API, the maximum time frame for viewing earthquake data is a year. The window to the left will display a list of preset earthquake sequences that come with VolcanoVR as default, and presets you have defined (see below section for more info on how to add your own). 

The presets provided by default are as follows:
1. [Taupo 2019 Unrest Sequence](https://doi.org/10.1029/2021GC009803)
2. [Tarawera 2019 Dyke Intrusion](https://doi.org/10.3389/feart.2020.606992)

## How do I view my own TVZ earthquake data?
VolcanoVR possesses the ability to load and view your own volcanic earthquake data in 3D VR. When you first run VolcanoVR, a Presets folder in the installation/project root directory is created, containing a couple already-provided presets. This folder is where you will place all of your preset files. A snippet of a preset file is as follows:

```
code="taupo_2019"
name="Taupo 2019"
latitude=-38.792992
longitude=175.906243
width=60
earthquakes:
2019-01-02T01:01:01.522000Z,175.65916,-38.86101,6237,0.767647
2019-01-02T01:01:00.709000Z,175.65916,-38.86101,6237,0.809444
2019-01-02T06:14:52.031000Z,175.71062,-38.86039,8306,2.898469
```

The file name should be the code followed by `.eqc` (for **E**arth**Q**uake **C**atalogue). The name field is what it would display as in the Presets list within VolcanoVR. You then need to set the latitude and longitude of the **center** of the region, followed by the region width (in km) in the next field. Note these numerical fields are not surrounded by quotation marks like `code` and `name` are. The next line is simply `earthquakes:` followed by a [CSV](https://en.wikipedia.org/wiki/Comma-separated_values) list of earthquakes, with time in **UTC** (see examples) as the first parameter, then **longitude** followed by **latitude** (in WGS84). Next is the depth of the earthquake in meters, then finally the magnitude of the event.

If you have done this all correctly and as shown, then your earthquake data will appear in the Presets menu within VolcanoVR!

## Acknowledgements
This project was funded by and developed/tested in collaboration with the [ECLIPSE Project](https://sites.google.com/view/eclipse-supervolcanoes/) and [GNS Science](https://www.gns.cri.nz/).